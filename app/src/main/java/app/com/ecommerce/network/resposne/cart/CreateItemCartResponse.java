package app.com.ecommerce.network.resposne.cart;

public class CreateItemCartResponse {
    private int id;
    private int status;
   private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
