package app.com.ecommerce.network.resposne.digitalpayments;

import java.util.ArrayList;

public class DigitalPaymentRespponse {
    private int status;
    private ArrayList<OrderedAccounts> ordered_accounts;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<OrderedAccounts> getOrdered_accounts() {
        return ordered_accounts;
    }

    public void setOrdered_accounts(ArrayList<OrderedAccounts> ordered_accounts) {
        this.ordered_accounts = ordered_accounts;
    }
}
