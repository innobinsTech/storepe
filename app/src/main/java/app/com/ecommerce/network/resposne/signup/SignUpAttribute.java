package app.com.ecommerce.network.resposne.signup;

import java.util.List;

public class SignUpAttribute {
    private String name;
    private String mobile;
    private Boolean active;
    private String postal_code;
    private String auth_token;
    private String company_mobile_number;
    private List<BranchesData> branches;
    private List<SalesPersonsData> salespersons;
    private List<SalesPersonsData> statuses;
    private List<SalesPersonsData> material_centers;
    private List<SalesPersonsData> sales_type;
    private List<SalesPersonsData> voucher_series_for_sales_order_final;

    public List<SalesPersonsData> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<SalesPersonsData> statuses) {
        this.statuses = statuses;
    }

    public List<SalesPersonsData> getMaterial_centers() {
        return material_centers;
    }

    public void setMaterial_centers(List<SalesPersonsData> material_centers) {
        this.material_centers = material_centers;
    }

    public List<SalesPersonsData> getSales_type() {
        return sales_type;
    }

    public void setSales_type(List<SalesPersonsData> sales_type) {
        this.sales_type = sales_type;
    }

    public List<SalesPersonsData> getVoucher_series_for_sales_order_final() {
        return voucher_series_for_sales_order_final;
    }

    public void setVoucher_series_for_sales_order_final(List<SalesPersonsData> voucher_series_for_sales_order_final) {
        this.voucher_series_for_sales_order_final = voucher_series_for_sales_order_final;
    }

    public List<BranchesData> getBranches() {
        return branches;
    }

    public void setBranches(List<BranchesData> branches) {
        this.branches = branches;
    }

    public List<SalesPersonsData> getSalespersons() {
        return salespersons;
    }

    public void setSalespersons(List<SalesPersonsData> salespersons) {
        this.salespersons = salespersons;
    }

    public String getCompany_mobile_number() {
        return company_mobile_number;
    }

    public void setCompany_mobile_number(String company_mobile_number) {
        this.company_mobile_number = company_mobile_number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}
