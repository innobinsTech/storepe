package app.com.ecommerce.network.resposne.digitalpayments;

import java.util.ArrayList;


public class OrderedAccounts {
    private String group_name;
    private ArrayList<UserAccountData> data;

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public ArrayList<UserAccountData> getData() {
        return data;
    }

    public void setData(ArrayList<UserAccountData> data) {
        this.data = data;
    }
}
