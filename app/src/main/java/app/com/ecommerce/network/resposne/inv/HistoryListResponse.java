package app.com.ecommerce.network.resposne.inv;


public class HistoryListResponse {
    SaleVouchers sale_vouchers;
    SaleVouchers purchase_vouchers;
    int status;
    String message;

    public SaleVouchers getSale_vouchers() {
        return sale_vouchers;
    }

    public void setSale_vouchers(SaleVouchers sale_vouchers) {
        this.sale_vouchers = sale_vouchers;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public SaleVouchers getPurchase_vouchers() {
        return purchase_vouchers;
    }

    public void setPurchase_vouchers(SaleVouchers purchase_vouchers) {
        this.purchase_vouchers = purchase_vouchers;
    }
}
