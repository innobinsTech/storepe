package app.com.ecommerce.network.resposne.myorder;

public class Data {

    private String type;
    private String id;
    private MyOrderAttributes attributes;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MyOrderAttributes getMyOrderAttributes() {
        return attributes;
    }

    public void setMyOrderAttributes(MyOrderAttributes myOrderAttributes) {
        this.attributes = myOrderAttributes;
    }
}