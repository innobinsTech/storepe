package app.com.ecommerce.network.request;

public class ItemCartRequest {
    private String company_id;
    private String user_id;
    private String item_id;
    private String quantity;
    private String packaging_category_id;

    public String getPackaging_category_id() {
        return packaging_category_id;
    }

    public void setPackaging_category_id(String packaging_category_id) {
        this.packaging_category_id = packaging_category_id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
