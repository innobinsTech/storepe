package app.com.ecommerce.network.resposne.useraccounts;

public class UserAccountAttributes {
    private int id;
    private String name;
    private String pincode;
    private String account_group;
    private String address;
    private String city;
    private String country;
    private String state;
    private String mobile_number;
    private String email;
    private String company;
    private String undefined;
//    private int amount;
    private int account_master_group_id;
    private String[] user_ids;
    private String[] branch_ids;
    private String status;

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccount_group() {
        return account_group;
    }

    public void setAccount_group(String account_group) {
        this.account_group = account_group;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getUndefined() {
        return undefined;
    }

    public void setUndefined(String undefined) {
        this.undefined = undefined;
    }



    public int getAccount_master_group_id() {
        return account_master_group_id;
    }

    public void setAccount_master_group_id(int account_master_group_id) {
        this.account_master_group_id = account_master_group_id;
    }

    public String[] getUser_ids() {
        return user_ids;
    }

    public void setUser_ids(String[] user_ids) {
        this.user_ids = user_ids;
    }

    public String[] getBranch_ids() {
        return branch_ids;
    }

    public void setBranch_ids(String[] branch_ids) {
        this.branch_ids = branch_ids;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
