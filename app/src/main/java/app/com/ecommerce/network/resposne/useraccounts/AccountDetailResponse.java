package app.com.ecommerce.network.resposne.useraccounts;

public class AccountDetailResponse {
   private AccountDetail account;
   private int status;
   private String message;

    public AccountDetail getAccount() {
        return account;
    }

    public void setAccount(AccountDetail account) {
        this.account = account;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
