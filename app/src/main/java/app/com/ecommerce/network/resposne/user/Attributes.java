
package app.com.ecommerce.network.resposne.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("active")
    @Expose
    private Boolean active;
    @SerializedName("postal_code")
    @Expose
    private String postalCode;
    @SerializedName("auth_token")
    @Expose
    private String authToken;
    @SerializedName("house_no")
    @Expose
    private String houseNo;
    @SerializedName("appartment")
    @Expose
    private String appartment;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("district")
    @Expose
    private String district;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("reference_number")
    @Expose
    private String referenceNumber;
    @SerializedName("bank_name")
    @Expose
    private String bankName;
    @SerializedName("bank_account_number")
    @Expose
    private String bankAccountNumber;
    @SerializedName("bank_address")
    @Expose
    private String bankAddress;
    @SerializedName("ifsc_code")
    @Expose
    private String ifscCode;
    @SerializedName("paytm_number")
    @Expose
    private String paytmNumber;
    @SerializedName("google_pay_number")
    @Expose
    private String googlePayNumber;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("date_of_anniversary")
    @Expose
    private String dateOfAnniversary;
    @SerializedName("club")
    @Expose
    private String club;
    @SerializedName("tier")
    @Expose
    private String tier;
    @SerializedName("id_proof")
    @Expose
    private String idProof;
    @SerializedName("id_proof_image")
    @Expose
    private String idProofImage;
    @SerializedName("userid")
    @Expose
    private String userid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getAppartment() {
        return appartment;
    }

    public void setAppartment(String appartment) {
        this.appartment = appartment;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getBankAddress() {
        return bankAddress;
    }

    public void setBankAddress(String bankAddress) {
        this.bankAddress = bankAddress;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getPaytmNumber() {
        return paytmNumber;
    }

    public void setPaytmNumber(String paytmNumber) {
        this.paytmNumber = paytmNumber;
    }

    public String getGooglePayNumber() {
        return googlePayNumber;
    }

    public void setGooglePayNumber(String googlePayNumber) {
        this.googlePayNumber = googlePayNumber;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDateOfAnniversary() {
        return dateOfAnniversary;
    }

    public void setDateOfAnniversary(String dateOfAnniversary) {
        this.dateOfAnniversary = dateOfAnniversary;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    public String getIdProof() {
        return idProof;
    }

    public void setIdProof(String idProof) {
        this.idProof = idProof;
    }

    public String getIdProofImage() {
        return idProofImage;
    }

    public void setIdProofImage(String idProofImage) {
        this.idProofImage = idProofImage;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
