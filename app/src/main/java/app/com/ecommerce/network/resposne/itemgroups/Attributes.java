package app.com.ecommerce.network.resposne.itemgroups;

public class Attributes {
    public Integer id;
    public String name;
    public String item_group_image_thumb_url;
    public String item_group_image_cover_url;
    public Boolean undefined;

    public String getItem_group_image_thumb_url() {
        return item_group_image_thumb_url;
    }

    public void setItem_group_image_thumb_url(String item_group_image_thumb_url) {
        this.item_group_image_thumb_url = item_group_image_thumb_url;
    }

    public String getItem_group_image_cover_url() {
        return item_group_image_cover_url;
    }

    public void setItem_group_image_cover_url(String item_group_image_cover_url) {
        this.item_group_image_cover_url = item_group_image_cover_url;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getUndefined() {
        return undefined;
    }

    public void setUndefined(Boolean undefined) {
        this.undefined = undefined;
    }
}
