package app.com.ecommerce.network.resposne.item;

public class PackagingCategories {
    private int packaging_category_id;
    private int item_unit_id;
    private int item_id;
    private int tax_category_id;
    private String name;
    private String code;
    private String hsn_number;
    private String description;
    private String discount_value;
    private String discount_percent;
    private Double sales_price;
    private Double mrp;
    private Double purchase_price;
    private String colour;
    private String itemAmount;
    private ItemImage item_image;

    public String getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(String itemAmount) {
        this.itemAmount = itemAmount;
    }

    public int getPackaging_category_id() {
        return packaging_category_id;
    }

    public void setPackaging_category_id(int packaging_category_id) {
        this.packaging_category_id = packaging_category_id;
    }

    public int getItem_unit_id() {
        return item_unit_id;
    }

    public void setItem_unit_id(int item_unit_id) {
        this.item_unit_id = item_unit_id;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public int getTax_category_id() {
        return tax_category_id;
    }

    public void setTax_category_id(int tax_category_id) {
        this.tax_category_id = tax_category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHsn_number() {
        return hsn_number;
    }

    public void setHsn_number(String hsn_number) {
        this.hsn_number = hsn_number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDiscount_value() {
        return discount_value;
    }

    public void setDiscount_value(String discount_value) {
        this.discount_value = discount_value;
    }

    public String getDiscount_percent() {
        return discount_percent;
    }

    public void setDiscount_percent(String discount_percent) {
        this.discount_percent = discount_percent;
    }

    public Double getSales_price() {
        return sales_price;
    }

    public void setSales_price(Double sales_price) {
        this.sales_price = sales_price;
    }

    public Double getMrp() {
        return mrp;
    }

    public void setMrp(Double mrp) {
        this.mrp = mrp;
    }

    public Double getPurchase_price() {
        return purchase_price;
    }

    public void setPurchase_price(Double purchase_price) {
        this.purchase_price = purchase_price;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public ItemImage getItem_image() {
        return item_image;
    }

    public void setItem_image(ItemImage item_image) {
        this.item_image = item_image;
    }
}
