
package app.com.ecommerce.network.resposne.inv;

import java.util.List;

public class OrderedItem {


    private Integer id;
    private String name;
    private String description;
    private String default_unit;
    private String sale_unit;
    private Integer tax_category_id;
    private String tax_category_name;
    private Double mrp;
    private Double sales_price_main;
    private List<String> barcode = null;
    private Double discount_value;
    private Double discount_percentage;
    private Double total_amount;
    String image;




    double rate_item;

// below property is not beloging from api


    public int quantity = 1;
    public double total = 0.0;
    public double wTotal = 0.0;
    public double itemTotal = 0.0;
    public double discount = 0.0;
    public double rate;
    public double value;
    public List<String> serial_number;
    public Integer item_id;
    public double tax;

    String generic_serialno;
    String item_code;
    public Double mrp_withouttax;


    public void setOrderedItem() {
        try {
            rate = sales_price_main;
            value = getTotalDiscount();
            serial_number = barcode;
            item_id = id;
            if (tax_category_name.equals("Exempt")) {
                tax = 0;
            } else {
                String name = tax_category_name.replace("GST ", "");
                name = name.replace("%", "");
                tax = Double.valueOf(name);
//            tax= (1 + Double.valueOf(name) / 100);
            }
        }catch (IllegalStateException e1){
            e1.printStackTrace();
        }catch (Exception e1){
            e1.printStackTrace();
        }catch (Error e1){
            e1.printStackTrace();
        }catch (Throwable e1){
            e1.printStackTrace();
        }



    }


    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTotalPrice() {
        return total;
    }

    public void setTotalPrice(double totalPrice) {
        this.total = totalPrice;
    }

    public double getTotalPriceWithoutTax() {
        return itemTotal;
    }

    public void setTotalPriceWithoutTax(double totalPrice) {
        this.itemTotal = totalPrice;
    }

    public double getTotalDiscount() {
        return discount;
    }

    public void setTotalDiscount(double totalDiscount) {
        this.discount = totalDiscount;
    }

    public double getTotalTaxIncludePrice() {
        return wTotal;
    }

    public void setTotalTaxIncludePrice(double totalTaxIncludePrice) {
        this.wTotal = totalTaxIncludePrice;
    }


    public double getTax() {
        double returnTax = 1;
        try {
            if (tax_category_name.equals("Exempt")) {
                return 1;
            } else {
                String name = tax_category_name.replace("GST ", "");
                name = name.replace("%", "");
                returnTax=(1 + Double.valueOf(name) / 100);
                return returnTax;
            }
        }catch (IllegalStateException e1){
            e1.printStackTrace();
        }catch (Exception e1){
            e1.printStackTrace();
        }catch (Error e1){
            e1.printStackTrace();
        }catch (Throwable e1){
            e1.printStackTrace();
        }
        return returnTax;

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDefault_unit() {
        return default_unit;
    }

    public void setDefault_unit(String default_unit) {
        this.default_unit = default_unit;
    }

    public String getSale_unit() {
        return sale_unit;
    }

    public void setSale_unit(String sale_unit) {
        this.sale_unit = sale_unit;
    }

    public Integer getTax_category_id() {
        return tax_category_id;
    }

    public void setTax_category_id(Integer tax_category_id) {
        this.tax_category_id = tax_category_id;
    }

    public String getTax_category_name() {
        return tax_category_name;
    }

    public void setTax_category_name(String tax_category_name) {
        this.tax_category_name = tax_category_name;
    }

    public Double getMrp() {
        return mrp;
    }

    public void setMrp(Double mrp) {
        this.mrp = mrp;
    }

    public Double getSales_price_main() {
        return sales_price_main;
    }

    public void setSales_price_main(Double sales_price_main) {
        this.sales_price_main = sales_price_main;
    }

    public List<String> getBarcode() {
        return barcode;
    }

    public void setBarcode(List<String> barcode) {
        this.barcode = barcode;
    }

    public Double getDiscount_value() {
        return discount_value;
    }

    public void setDiscount_value(Double discount_value) {
        this.discount_value = discount_value;
    }

    public double getTaxIncludePrice() {
        return wTotal;
    }

    public void setTaxIncludePrice(double wTotal) {
        this.wTotal = wTotal;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public List<String> getSerial_no() {
        return serial_number;
    }

    public void setSerial_no(List<String> serial_no) {
        this.serial_number = serial_no;
    }

    public Integer getItem_id() {
        return item_id;
    }

    public void setItem_id(Integer item_id) {
        this.item_id = item_id;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }


    public double getRate_item() {
        return rate_item;
    }

    public void setRate_item(double rate_item) {
        this.rate_item = rate_item;
    }


    public String getGeneric_serialno() {
        return generic_serialno;
    }

    public void setGeneric_serialno(String generic_serialno) {
        this.generic_serialno = generic_serialno;
    }

    public Double getMrp_withouttax() {
        return mrp_withouttax;
    }

    public void setMrp_withouttax(Double mrp_withouttax) {
        this.mrp_withouttax = mrp_withouttax;
    }

    public Double getDiscount_percentage() {
        return discount_percentage;
    }

    public void setDiscount_percentage(Double discount_percentage) {
        this.discount_percentage = discount_percentage;
    }

    public String getItem_code() {
        return item_code;
    }

    public void setItem_code(String item_code) {
        this.item_code = item_code;
    }


    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }


    public Double getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(Double total_amount) {
        this.total_amount = total_amount;
    }
}
