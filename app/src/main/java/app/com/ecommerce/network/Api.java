package app.com.ecommerce.network;


import java.util.Map;

import app.com.ecommerce.network.request.CreateAccountRequest;
import app.com.ecommerce.network.request.ItemCartRequest;
import app.com.ecommerce.network.request.ItemPaginationRequest;
import app.com.ecommerce.network.request.OrderListRequest;
import app.com.ecommerce.network.resposne.SuccessResponse;
import app.com.ecommerce.network.resposne.cart.CreateItemCartResponse;
import app.com.ecommerce.network.resposne.cart.GetItemCartResponse;
import app.com.ecommerce.network.resposne.digitalpayments.DigitalPaymentRespponse;
import app.com.ecommerce.network.resposne.inv.HistoryListResponse;
import app.com.ecommerce.network.resposne.inv.OnGoingListResponse;
import app.com.ecommerce.network.resposne.item.GetItemResponse;
import app.com.ecommerce.network.resposne.itemgroups.AccountGroupResponse;
import app.com.ecommerce.network.resposne.myorder.MyOrderListResponse;
import app.com.ecommerce.network.resposne.points.PointResponse;
import app.com.ecommerce.network.resposne.salevoucher.CreateSaleVoucherResponse;
import app.com.ecommerce.network.resposne.scan.ScanResponse;
import app.com.ecommerce.network.resposne.signup.ResendOtpResponse;
import app.com.ecommerce.network.resposne.signup.SignUpResponse;
import app.com.ecommerce.network.resposne.user.GetUserResponse;
import app.com.ecommerce.network.resposne.useraccounts.AccountDetailResponse;
import app.com.ecommerce.network.resposne.useraccounts.UserAccountRespponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {

    @POST("api/v1/company_signup")
    Call<SignUpResponse> signup(@Body Map login);

    @POST("api/v1/company_login")
    Call<SignUpResponse> login(@Body Map login);

    @POST("/api/v1/barcode_company")
    Call<ScanResponse> scan(@Body Map login);

    @POST("api/v1/company_verify_otp")
    Call<SignUpResponse> otp(@Body Map login);

    @PATCH("api/v1/company_resend_otp")
    Call<ResendOtpResponse> resendOtp(@Body Map login);

    @GET("api/v1/item/{id}")
    Call<GetItemResponse> getItems(@Path("id") String id, @Query("date") String date);

    @GET("api/v1/company/company_data_report_records/{id}")
    Call<PointResponse> getPoint(@Path("id") String company_id, @Query("id") String id, @Query("account_name") String account_name, @Query("start_date") String start_date, @Query("end_date") String end_date);

    @GET("api/v1/company_get_user/{id}")
    Call<GetUserResponse> getUser(@Path("id") String id);

  /*  @POST("api/v1/sale_voucher/{id}")
    Call<CreateSaleVoucherResponse> orderSubmit(@Body Map createItem, @Path("id") String id);*/

    @POST("api/v1/sales_order_final/{id}")
    Call<CreateSaleVoucherResponse> orderSubmit(@Body Map createItem, @Path("id") String id);

    @GET("api/v1/user_accounts/{id}")
    Call<UserAccountRespponse> getAccountsList(@Path("id") String id);

    @POST("api/v1/account")
    Call<SuccessResponse> createAccount(@Body CreateAccountRequest createItem);

    @GET("api/v1/account_detail/{id}")
    Call<AccountDetailResponse> getAccountDetails(@Path("id") String id);

    @PATCH("api/v1/account/{id}")
    Call<SuccessResponse> updateAccount(@Body CreateAccountRequest createItem, @Path("id") String id);

    @POST("api/v1/paginated_items")
    Call<GetItemResponse> getItemsPagination(@Body Map request);

    @GET("api/v1/company_item_groups/{id}")
    Call<AccountGroupResponse> getItemGroup(@Path("id") String id);

    @POST("api/v1/company_paginate_items")
    Call<GetItemResponse> getItemsGroupBy(@Body ItemPaginationRequest request);

    @POST("api/v1/company_sales_order_finals")
    Call<MyOrderListResponse> getOrderHistoryList(@Body OrderListRequest request);

    @POST("api/v1/get_sales_order_final_not_delivered_or_cancelled")
    Call<MyOrderListResponse> getPendingOrderLIst(@Body OrderListRequest request);

    @POST("api/v1/create_item_cart")
    Call<CreateItemCartResponse> createItemCart(@Body ItemCartRequest request);

    @POST("api/v1/get_item_cart_paginated")
    Call<GetItemCartResponse> getItemsCart(@Body ItemPaginationRequest request);


    @POST("api/v1/sales_order_final/{id}")
    Call<SuccessResponse> sale(@Path("id") String id, @Body Map payload);



    @POST("api/v1/get_sales_order_final_not_delivered_or_cancelled")
    Call<OnGoingListResponse> getOnGoingList(@Body Map map);


    @POST("api/v1/get_sales_order_final_delivered_or_cancelled")
    Call<HistoryListResponse> getHistory(@Body Map map);



    @POST("api/v1/update_sales_order_final_item")
    Call<SuccessResponse> updateOrder(@Body Map map);


    @GET("api/v1/invoice_notification/{id}")
    Call<SuccessResponse> invoiceNotification(@Path("id") String userId,
                                              @Query("vType") String voucher_type,
                                              @Query("type") String type);

    @GET("api/v1/digital_payment/{id}")
    Call<DigitalPaymentRespponse> getDigitalPaymentList(@Path("id") String id);

}