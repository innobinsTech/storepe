package app.com.ecommerce.network.resposne.useraccounts;

public class AccountDetailData {
    private UserAccountAttributes attributes;
    private String type;
    private int id;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserAccountAttributes getAttributes() {
        return attributes;
    }

    public void setAttributes(UserAccountAttributes attributes) {
        this.attributes = attributes;
    }
}
