package app.com.ecommerce.network.resposne.myorder;

public class MyOrderListResponse {

    private String message;
    private int status;
    private SaleVouchers sale_vouchers;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public SaleVouchers getSale_vouchers() {
        return sale_vouchers;
    }

    public void setSale_vouchers(SaleVouchers sale_vouchers) {
        this.sale_vouchers = sale_vouchers;
    }
}