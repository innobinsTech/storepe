package app.com.ecommerce.network.request;

public class CreateAccount {
    private int company_id;
    private String name;
    private String address;
    private String city;
    private String country;
    private String state;
    private String mobile_number;
    private String email;
    private String company;
    private String pincode;
    private String account_master_group_id;
    private String[] branch_ids;
    private String[] user_ids;
    private String[] sales_person_id;

    public String[] getBranch_ids() {
        return branch_ids;
    }

    public void setBranch_ids(String[] branch_ids) {
        this.branch_ids = branch_ids;
    }

    public String[] getUser_ids() {
        return user_ids;
    }

    public void setUser_ids(String[] user_ids) {
        this.user_ids = user_ids;
    }

    public String[] getSales_person_id() {
        return sales_person_id;
    }

    public void setSales_person_id(String[] sales_person_id) {
        this.sales_person_id = sales_person_id;
    }

    public String getAccount_master_group_id() {
        return account_master_group_id;
    }

    public void setAccount_master_group_id(String account_master_group_id) {
        this.account_master_group_id = account_master_group_id;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
