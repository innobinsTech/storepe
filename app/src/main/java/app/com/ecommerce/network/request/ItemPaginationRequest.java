package app.com.ecommerce.network.request;

public class ItemPaginationRequest {
    private String company_id;
    private String limit;
    private String page_no;
    private String item_group_id;
    private String user_id;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getItem_group_id() {
        return item_group_id;
    }

    public void setItem_group_id(String item_group_id) {
        this.item_group_id = item_group_id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getPage_no() {
        return page_no;
    }

    public void setPage_no(String page_no) {
        this.page_no = page_no;
    }
}
