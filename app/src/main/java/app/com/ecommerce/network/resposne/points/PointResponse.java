package app.com.ecommerce.network.resposne.points;

import java.util.ArrayList;

public class PointResponse {
    ArrayList<Company_report> company_report;
    String message;
    int status;
    double closing_balance;

    public double getClosing_balance() {
        return closing_balance;
    }

    public void setClosing_balance(double closing_balance) {
        this.closing_balance = closing_balance;
    }

    public ArrayList<Company_report> getCompany_report() {
        return company_report;
    }

    public void setCompany_report(ArrayList<Company_report> company_report) {
        this.company_report = company_report;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}


