package app.com.ecommerce.network.resposne.inv;


import java.util.List;

public class Attributes {


    private String date;
    private String time;
    private String voucher_series;
    private Integer companyId;
    private Integer id;
    private Integer sale_type_id;
    private Integer account_master_id;
    private Object shipped_to_id;
    private String shipped_to_name;
    private Integer material_center_id;
    private String sale_type;
    private String account_master;
    private String material_center;
    private String payment_type;
    private Double total;
    private Object items_amount;
    private Object bill_sundries_amount;

    private String attachment;
    private Object pos;
    private List<OrderedItem> voucher_items ;
    String deliveryType="Home Delivery";

    int points_used;

    double paid_amount;
    double billDiscount;
    double couponDiscount;
    String couponDiscountCode;

    String returnstatus;
    String invoice_html;
    String three_inch_invoice;
    String four_inch_invoice;
    double invoice_discount;
    String mobile_number;
    String voucher_number;
    boolean without_tax;
    boolean used_credits;

    String total_amount;
    String status_name;

    double discount_val;


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getVoucher_series() {
        return voucher_series;
    }

    public void setVoucher_series(String voucher_series) {
        this.voucher_series = voucher_series;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSale_type_id() {
        return sale_type_id;
    }

    public void setSale_type_id(Integer sale_type_id) {
        this.sale_type_id = sale_type_id;
    }

    public Integer getAccount_master_id() {
        return account_master_id;
    }

    public void setAccount_master_id(Integer account_master_id) {
        this.account_master_id = account_master_id;
    }

    public Object getShipped_to_id() {
        return shipped_to_id;
    }

    public void setShipped_to_id(Object shipped_to_id) {
        this.shipped_to_id = shipped_to_id;
    }

    public String getShipped_to_name() {
        return shipped_to_name;
    }

    public void setShipped_to_name(String shipped_to_name) {
        this.shipped_to_name = shipped_to_name;
    }

    public Integer getMaterial_center_id() {
        return material_center_id;
    }

    public void setMaterial_center_id(Integer material_center_id) {
        this.material_center_id = material_center_id;
    }

    public String getSale_type() {
        return sale_type;
    }

    public void setSale_type(String sale_type) {
        this.sale_type = sale_type;
    }

    public String getAccount_master() {
        return account_master;
    }

    public void setAccount_master(String account_master) {
        this.account_master = account_master;
    }

    public String getMaterial_center() {
        return material_center;
    }

    public void setMaterial_center(String material_center) {
        this.material_center = material_center;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Object getItems_amount() {
        return items_amount;
    }

    public void setItems_amount(Object items_amount) {
        this.items_amount = items_amount;
    }

    public Object getBill_sundries_amount() {
        return bill_sundries_amount;
    }

    public void setBill_sundries_amount(Object bill_sundries_amount) {
        this.bill_sundries_amount = bill_sundries_amount;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public Object getPos() {
        return pos;
    }

    public void setPos(Object pos) {
        this.pos = pos;
    }

    public List<OrderedItem> getVoucher_items() {
        return voucher_items;
    }

    public void setVoucher_items(List<OrderedItem> voucher_items) {
        this.voucher_items = voucher_items;
    }

    public int getPoints_used() {
        return points_used;
    }

    public void setPoints_used(int points_used) {
        this.points_used = points_used;
    }

    public double getPaid_amount() {
        return paid_amount;
    }

    public void setPaid_amount(double paid_amount) {
        this.paid_amount = paid_amount;
    }

    public double getBillDiscount() {
        return billDiscount;
    }

    public void setBillDiscount(double billDiscount) {
        this.billDiscount = billDiscount;
    }

    public double getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(double couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public String getCouponDiscountCode() {
        return couponDiscountCode;
    }

    public void setCouponDiscountCode(String couponDiscountCode) {
        this.couponDiscountCode = couponDiscountCode;
    }

    public String getReturnstatus() {
        return returnstatus;
    }

    public void setReturnstatus(String returnstatus) {
        this.returnstatus = returnstatus;
    }

    public String getInvoice_html() {
        return invoice_html;
    }

    public void setInvoice_html(String invoice_html) {
        this.invoice_html = invoice_html;
    }

    public double getInvoice_discount() {
        return invoice_discount;
    }

    public void setInvoice_discount(double invoice_discount) {
        this.invoice_discount = invoice_discount;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getVoucher_number() {
        return voucher_number;
    }

    public void setVoucher_number(String voucher_number) {
        this.voucher_number = voucher_number;
    }

    public boolean isWithout_tax() {
        return without_tax;
    }

    public void setWithout_tax(boolean without_tax) {
        this.without_tax = without_tax;
    }

    public boolean isUsed_credits() {
        return used_credits;
    }

    public void setUsed_credits(boolean used_credits) {
        this.used_credits = used_credits;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public double getDiscount_val() {
        return discount_val;
    }

    public void setDiscount_val(double discount_val) {
        this.discount_val = discount_val;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }

    public String getThree_inch_invoice() {
        return three_inch_invoice;
    }

    public void setThree_inch_invoice(String three_inch_invoice) {
        this.three_inch_invoice = three_inch_invoice;
    }

    public String getFour_inch_invoice() {
        return four_inch_invoice;
    }

    public void setFour_inch_invoice(String four_inch_invoice) {
        this.four_inch_invoice = four_inch_invoice;
    }
}
