package app.com.ecommerce.network.resposne.myorder;

import java.util.ArrayList;

public class MyOrderAttributes {
    private String date;
    private String time;
    private String status;
    private int company_id;
    private int id;
    private int sale_type_id;
    private int account_master_id;
    private int shipped_to_id;
    private int material_center_id;
    private String shipped_to_name;
    private String sale_type;
    private String material_center;
    private String account_master;
    private Double total_amount;
    private Double items_amount;
    private Double bill_sundries_amount;
    private ArrayList<VoucherItemData> voucher_items;
    private VoucherSeries voucher_series;
    private String branch_id;
    private String status_id;
    private String status_name;
    private String sales_people_id;
    private String payment_option_id;
    private String account_master_address;

    public String getAccount_master_address() {
        return account_master_address;
    }

    public void setAccount_master_address(String account_master_address) {
        this.account_master_address = account_master_address;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }

    public String getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(String branch_id) {
        this.branch_id = branch_id;
    }

    public String getStatus_id() {
        return status_id;
    }

    public void setStatus_id(String status_id) {
        this.status_id = status_id;
    }

    public String getSales_people_id() {
        return sales_people_id;
    }

    public void setSales_people_id(String sales_people_id) {
        this.sales_people_id = sales_people_id;
    }

    public String getPayment_option_id() {
        return payment_option_id;
    }

    public void setPayment_option_id(String payment_option_id) {
        this.payment_option_id = payment_option_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public VoucherSeries getVoucher_series() {
        return voucher_series;
    }

    public void setVoucher_series(VoucherSeries voucher_series) {
        this.voucher_series = voucher_series;
    }

    public ArrayList<VoucherItemData> getVoucher_items() {
        return voucher_items;
    }

    public void setVoucher_items(ArrayList<VoucherItemData> voucher_items) {
        this.voucher_items = voucher_items;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSale_type_id() {
        return sale_type_id;
    }

    public void setSale_type_id(int sale_type_id) {
        this.sale_type_id = sale_type_id;
    }

    public int getAccount_master_id() {
        return account_master_id;
    }

    public void setAccount_master_id(int account_master_id) {
        this.account_master_id = account_master_id;
    }

    public int getShipped_to_id() {
        return shipped_to_id;
    }

    public void setShipped_to_id(int shipped_to_id) {
        this.shipped_to_id = shipped_to_id;
    }

    public int getMaterial_center_id() {
        return material_center_id;
    }

    public void setMaterial_center_id(int material_center_id) {
        this.material_center_id = material_center_id;
    }

    public String getShipped_to_name() {
        return shipped_to_name;
    }

    public void setShipped_to_name(String shipped_to_name) {
        this.shipped_to_name = shipped_to_name;
    }

    public String getSale_type() {
        return sale_type;
    }

    public void setSale_type(String sale_type) {
        this.sale_type = sale_type;
    }

    public String getMaterial_center() {
        return material_center;
    }

    public void setMaterial_center(String material_center) {
        this.material_center = material_center;
    }

    public String getAccount_master() {
        return account_master;
    }

    public void setAccount_master(String account_master) {
        this.account_master = account_master;
    }

    public Double getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(Double total_amount) {
        this.total_amount = total_amount;
    }

    public Double getItems_amount() {
        return items_amount;
    }

    public void setItems_amount(Double items_amount) {
        this.items_amount = items_amount;
    }

    public Double getBill_sundries_amount() {
        return bill_sundries_amount;
    }

    public void setBill_sundries_amount(Double bill_sundries_amount) {
        this.bill_sundries_amount = bill_sundries_amount;
    }
}