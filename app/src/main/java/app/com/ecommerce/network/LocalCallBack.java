package app.com.ecommerce.network;


import org.greenrobot.eventbus.EventBus;

import app.com.ecommerce.utils.ParameterConstant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LocalCallBack<T> implements Callback<T> {
    T t;
    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.code() == 200) {
            T body = response.body();
            EventBus.getDefault().post(body);
        } else {
            EventBus.getDefault().post(ParameterConstant.ERROR);
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        EventBus.getDefault().post(ParameterConstant.ERROR);
    }
}