package app.com.ecommerce.network.resposne.item;

import java.util.List;

public class ItemPaginateResponse {
    private int status;
    private String message;
    private List<PaginateAttributes> items;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PaginateAttributes> getItems() {
        return items;
    }

    public void setItems(List<PaginateAttributes> items) {
        this.items = items;
    }
}
