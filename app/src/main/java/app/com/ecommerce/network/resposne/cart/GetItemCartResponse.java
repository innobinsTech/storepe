package app.com.ecommerce.network.resposne.cart;


import app.com.ecommerce.network.resposne.item.ItemsObject;

public class GetItemCartResponse {
    public int status;
    public String message;
    private ItemsObject items;

    public ItemsObject getItems() {
        return items;
    }

    public void setItems(ItemsObject items) {
        this.items = items;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}