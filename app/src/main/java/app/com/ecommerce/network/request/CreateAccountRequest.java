package app.com.ecommerce.network.request;

public class CreateAccountRequest {
    private CreateAccount account;

    public CreateAccount getAccount() {
        return account;
    }

    public void setAccount(CreateAccount account) {
        this.account = account;
    }
}
