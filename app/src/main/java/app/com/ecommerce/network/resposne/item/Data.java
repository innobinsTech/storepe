package app.com.ecommerce.network.resposne.item;

public class Data {
    String type;
    String id;
    Attributes attributes;
    String quantity="0";
    public String packaging_category_id;
    private Boolean plusMinus = true;



    public String getPackaging_category_id() {
        return packaging_category_id;
    }

    public void setPackaging_category_id(String packaging_category_id) {
        this.packaging_category_id = packaging_category_id;
    }

    public Boolean getPlusMinus() {
        return plusMinus;
    }

    public void setPlusMinus(Boolean plusMinus) {
        this.plusMinus = plusMinus;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}