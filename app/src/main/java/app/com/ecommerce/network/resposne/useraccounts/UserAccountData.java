package app.com.ecommerce.network.resposne.useraccounts;

public class UserAccountData {
    private String type;
    private String id;
    private UserAccountAttributes attributes;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserAccountAttributes getAttributes() {
        return attributes;
    }

    public void setAttributes(UserAccountAttributes attributes) {
        this.attributes = attributes;
    }
}
