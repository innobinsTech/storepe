package app.com.ecommerce.network.resposne.digitalpayments;



public class UserAccountData {
    private String type;
    private String id;
    private DigitalPaymentAttributes attributes;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DigitalPaymentAttributes getAttributes() {
        return attributes;
    }

    public void setAttributes(DigitalPaymentAttributes attributes) {
        this.attributes = attributes;
    }
}
