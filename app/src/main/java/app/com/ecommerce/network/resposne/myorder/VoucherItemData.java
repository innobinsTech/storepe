package app.com.ecommerce.network.resposne.myorder;

public class VoucherItemData {
    private int id;
    private int item_id;
    private String item;
    private int quantity;
    private Double price;
    private Double total_amount;
    private Double discount;
    private Double mrp;
    private Double rate_item;
    private String item_unit;
    private String packaging_category_id;
    private String packaging_category_name;
    private String packaging_unit_id;
    private String packaging_unit;

    public String getPackaging_category_id() {
        return packaging_category_id;
    }

    public void setPackaging_category_id(String packaging_category_id) {
        this.packaging_category_id = packaging_category_id;
    }

    public String getPackaging_category_name() {
        return packaging_category_name;
    }

    public void setPackaging_category_name(String packaging_category_name) {
        this.packaging_category_name = packaging_category_name;
    }

    public String getPackaging_unit_id() {
        return packaging_unit_id;
    }

    public void setPackaging_unit_id(String packaging_unit_id) {
        this.packaging_unit_id = packaging_unit_id;
    }

    public String getPackaging_unit() {
        return packaging_unit;
    }

    public void setPackaging_unit(String packaging_unit) {
        this.packaging_unit = packaging_unit;
    }

    public String getItem_unit() {
        return item_unit;
    }

    public void setItem_unit(String item_unit) {
        this.item_unit = item_unit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(Double total_amount) {
        this.total_amount = total_amount;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getMrp() {
        return mrp;
    }

    public void setMrp(Double mrp) {
        this.mrp = mrp;
    }

    public Double getRate_item() {
        return rate_item;
    }

    public void setRate_item(Double rate_item) {
        this.rate_item = rate_item;
    }
}
