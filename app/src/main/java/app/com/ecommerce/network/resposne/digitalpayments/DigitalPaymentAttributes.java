package app.com.ecommerce.network.resposne.digitalpayments;

public class DigitalPaymentAttributes {

    private String name;
    private String state;
    private String type_of_dealer;
    private String mobile_number;
    private String company;
    private boolean undefined;
    private double amount;
    private double points;
    private int account_master_group_id;
    private String last_visited;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getType_of_dealer() {
        return type_of_dealer;
    }

    public void setType_of_dealer(String type_of_dealer) {
        this.type_of_dealer = type_of_dealer;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public boolean isUndefined() {
        return undefined;
    }

    public void setUndefined(boolean undefined) {
        this.undefined = undefined;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getPoints() {
        return points;
    }

    public void setPoints(double points) {
        this.points = points;
    }

    public int getAccount_master_group_id() {
        return account_master_group_id;
    }

    public void setAccount_master_group_id(int account_master_group_id) {
        this.account_master_group_id = account_master_group_id;
    }

    public String getLast_visited() {
        return last_visited;
    }

    public void setLast_visited(String last_visited) {
        this.last_visited = last_visited;
    }
}
