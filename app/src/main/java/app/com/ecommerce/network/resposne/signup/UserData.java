package app.com.ecommerce.network.resposne.signup;

public class UserData {
    private String type;
    private String id;
    private SignUpAttribute attributes;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SignUpAttribute getAttributes() {
        return attributes;
    }

    public void setAttributes(SignUpAttribute attributes) {
        this.attributes = attributes;
    }
}
