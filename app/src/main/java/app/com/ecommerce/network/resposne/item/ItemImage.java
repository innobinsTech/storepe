package app.com.ecommerce.network.resposne.item;

public class ItemImage {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private ItemImage thumb;
    private ItemImage cover;

    public ItemImage getThumb() {
        return thumb;
    }

    public void setThumb(ItemImage thumb) {
        this.thumb = thumb;
    }

    public ItemImage getCover() {
        return cover;
    }

    public void setCover(ItemImage cover) {
        this.cover = cover;
    }
}
