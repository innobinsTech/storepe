package app.com.ecommerce.network.resposne.useraccounts;

import java.util.ArrayList;

public class UserAccountRespponse {
    private int status;
    private String message;
    private ArrayList<OrderedAccounts> ordered_accounts;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<OrderedAccounts> getOrdered_accounts() {
        return ordered_accounts;
    }

    public void setOrdered_accounts(ArrayList<OrderedAccounts> ordered_accounts) {
        this.ordered_accounts = ordered_accounts;
    }
}
