package app.com.ecommerce.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.Map;

import app.com.ecommerce.R;
import app.com.ecommerce.activity.base.ApiCallFragment;
import app.com.ecommerce.adapter.OnGoingAdapter;
import app.com.ecommerce.events.CancelEvent;
import app.com.ecommerce.network.resposne.SuccessResponse;
import app.com.ecommerce.network.resposne.inv.OnGoingListResponse;
import app.com.ecommerce.utils.Helper;
import app.com.ecommerce.utils.MyProgressDialog;
import app.com.ecommerce.utils.ParameterConstant;
import app.com.ecommerce.utils.Preferences;
import me.ibrahimsn.lib.CirclesLoadingView;

public class OnGoingOrderFragment extends ApiCallFragment {

    EditText search;
    TextView from;
    LinearLayout fromLayout;
    TextView to;
    LinearLayout toLayout;
    LinearLayout close;
    OnGoingAdapter adapter;
    OnGoingListResponse response;
    RecyclerView recyclerView;
    CirclesLoadingView loader;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.on_going_frgment, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        loader = view.findViewById(R.id.loader);
        search = view.findViewById(R.id.search);
        from = view.findViewById(R.id.from);
        fromLayout = view.findViewById(R.id.fromLayout);
        to = view.findViewById(R.id.to);
        toLayout = view.findViewById(R.id.toLayout);
        close = view.findViewById(R.id.close);
//        date();
        close.setVisibility(View.GONE);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close.setVisibility(View.GONE);
                search.setText("");
            }
        });
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (search.getText().toString().isEmpty()) {
                    close.setVisibility(View.GONE);
                } else {
                    close.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    OnGoingAdapter.filteredList = response.getSale_vouchers().getData();
                    OnGoingAdapter.responseList = response.getSale_vouchers().getData();
                    adapter.getFilter().filter(s.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        loader.setVisibility(View.VISIBLE);
        Map map = new HashMap();
        map.put("id", Preferences.getInstance(getContext()).getUserId());
        map.put("company_id", ParameterConstant.COMPANY_ID);
        map.put("end_date", Helper.getCurrentDate());
        map.put("start_date", Helper.getPast10Date());
        apiCallBack(getApi().getOnGoingList(map));

        return view;
    }


    @Subscribe
    public void getList(OnGoingListResponse response) {
        loader.setVisibility(View.GONE);
        if (response.getStatus() == 200) {
            this.response = response;
            setAdapter(response);
        } else {
            getDialogFailed(response.getMessage());
        }
    }

    void setAdapter(OnGoingListResponse response) {
        recyclerView.setHasFixedSize(true);
        recyclerView.setFocusable(false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        adapter = new OnGoingAdapter(getActivity(), response.getSale_vouchers().getData());
        recyclerView.setAdapter(adapter);
    }



        /*void  date(){
        Preferences.getInstance(getActivity()).setStartDate(Helper.getPast10Date());
        Preferences.getInstance(getActivity()).setEndDate(Helper.getCurrentDate());
        from.setText(Preferences.getInstance(getActivity()).getStartDate());
        to.setText(Preferences.getInstance(getActivity()).getEndDate());
        fromLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.getInstance(getActivity()).setStartDate(Helper.getCurrentDate());
                Preferences.getInstance(getActivity()).setEndDate(Helper.getCurrentDate());
                dateDialog(from, to);
            }
        });
        toLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.getInstance(getActivity()).setStartDate(Helper.getCurrentDate());
                Preferences.getInstance(getActivity()).setEndDate(Helper.getCurrentDate());
                dateDialog(from, to);
            }
        });
    }
    public void dateDialog(TextView start,TextView end) {

        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().requestFeature(1);
        dialog.setContentView(R.layout.date_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);

        TextView from = dialog.findViewById(R.id.from);
        LinearLayout close = dialog.findViewById(R.id.close);
        TextView to = dialog.findViewById(R.id.to);
        Button submit = dialog.findViewById(R.id.submit);

        from.setText(Preferences.getInstance(getActivity()).getStartDate());
        to.setText(Preferences.getInstance(getActivity()).getEndDate());

        from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateSelection(from, "start", to);
            }
        });
        to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateSelection(to, "end", from);
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Date fromDate = null;
                Date endDate = null;
                try {
                    fromDate = sdf.parse(from.getText().toString());
                    endDate = sdf.parse(to.getText().toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (fromDate.getTime() > endDate.getTime()) {
                    Toast.makeText(getActivity(), "Enter valid date", Toast.LENGTH_SHORT).show();
                    return;
                }
                dialog.dismiss();
                start.setText(from.getText().toString());
                end.setText(to.getText().toString());
                Map map=new HashMap();
                map.put("id",Preferences.getInstance(getContext()).getUserId());
                map.put("company_id",ParameterConstant.COMPANY_ID);
                apiCallBack(getApi().getOnGoingList(map));
            }
        });

        dialog.show();
    }
    public void dateSelection(TextView v2, String str, TextView v3) {
        Calendar calendar = Calendar.getInstance();
//        String date = v3.getText().toString();
//        int year = Integer.parseInt(date.split("/")[2]);
//        int month = Integer.parseInt(date.split("/")[1]);
//        int day = Integer.parseInt(date.split("/")[0]);
//        calendar.set(year, month - 1, day);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                String d = new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(calendar.getTime());
                if (str.equals("start")) {
                    Preferences.getInstance(getActivity()).setStartDate(d);
                    v2.setText(d);
                } else if (str.equals("end")) {
                    Preferences.getInstance(getActivity()).setEndDate(d);
                    v2.setText(d);
                }
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }*/

    CancelEvent event;

    @Subscribe
    public void getEvent(CancelEvent event) {
        MyProgressDialog.getInstance(getActivity()).show();
        this.event = event;
        Map map = new HashMap();
        map.put("id", response.getSale_vouchers().getData().get(event.getPosition()).getAttributes().getId());
        map.put("company_id", ParameterConstant.COMPANY_ID);
        apiCallBack(getApi().updateOrder(map));
    }



    @Subscribe
    public void responseCancel(SuccessResponse response) {
        if (response.getStatus()==200){
           this. response.getSale_vouchers().getData().remove(event.getPosition());
           adapter.notifyDataSetChanged();
        }
    }

}
