package app.com.ecommerce.checkout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PaymentmodeList {
   private List<SelectedMode> payment_mode;

    public PaymentmodeList(List<SelectedMode> payment_mode) {
        this.payment_mode = payment_mode;
    }

    public List<SelectedMode> getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(List<SelectedMode> payment_mode) {
        this.payment_mode = payment_mode;
    }
}
