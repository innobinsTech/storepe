package app.com.ecommerce.checkout;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import app.com.ecommerce.R;
import app.com.ecommerce.network.resposne.digitalpayments.UserAccountData;

public class BillPaymentListAdapter extends RecyclerView.Adapter<BillPaymentListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<UserAccountData> data;
    private OnPaymentSelect onPaymentSelect;

    public BillPaymentListAdapter(Context context, ArrayList<UserAccountData> data, OnPaymentSelect onPaymentSelect) {
        this.context = context;
        this.data = data;
        this.onPaymentSelect = onPaymentSelect;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_bill, viewGroup, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.name.setText(data.get(position).getAttributes().getName());
        viewHolder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.name.setBackground(context.getDrawable(R.drawable.round_cash));
                viewHolder.price.setVisibility(View.VISIBLE);

            }
        });

        viewHolder.price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0) {
                    SelectedMode selectedMode = new SelectedMode(0.0,data.get(position).getId(),"Sales Order Final",data.get(position).getAttributes().getName());
                    onPaymentSelect.OnSelectedPayments(selectedMode, position);
                }else if(s.length()>0) {
                    SelectedMode selectedMode = new SelectedMode(Double.parseDouble(viewHolder.price.getText().toString()),data.get(position).getId(),"Sales Order Final",data.get(position).getAttributes().getName());
                    onPaymentSelect.OnSelectedPayments(selectedMode, position);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }


    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        EditText price;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            price = itemView.findViewById(R.id.price);
        }
    }
}