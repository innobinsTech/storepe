package app.com.ecommerce.checkout;

public class SelectedMode {

    public double amount;
    public String payment_account_id;
    public String voucher_type;
    public String payment_account_name;

    public SelectedMode(double amount, String payment_account_id, String voucher_type, String payment_account_name) {
        this.amount = amount;
        this.payment_account_id = payment_account_id;
        this.voucher_type = voucher_type;
        this.payment_account_name = payment_account_name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPayment_account_id() {
        return payment_account_id;
    }

    public void setPayment_account_id(String payment_account_id) {
        this.payment_account_id = payment_account_id;
    }

    public String getVoucher_type() {
        return voucher_type;
    }

    public void setVoucher_type(String voucher_type) {
        this.voucher_type = voucher_type;
    }

    public String getPayment_account_name() {
        return payment_account_name;
    }

    public void setPayment_account_name(String payment_account_name) {
        this.payment_account_name = payment_account_name;
    }
}
