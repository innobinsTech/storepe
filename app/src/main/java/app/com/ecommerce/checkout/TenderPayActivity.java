package app.com.ecommerce.checkout;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.annotation.IdRes;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import app.com.ecommerce.R;
import app.com.ecommerce.activity.HomeActivity;
import app.com.ecommerce.activity.OrderHistoryActivity;
import app.com.ecommerce.activity.base.BaseActivity;
import app.com.ecommerce.network.resposne.SuccessResponse;
import app.com.ecommerce.network.resposne.digitalpayments.DigitalPaymentRespponse;
import app.com.ecommerce.network.resposne.item.Attributes;
import app.com.ecommerce.network.resposne.item.Data;
import app.com.ecommerce.utils.AppUser;
import app.com.ecommerce.utils.MyProgressDialog;
import app.com.ecommerce.utils.ParameterConstant;
import app.com.ecommerce.utils.Preferences;
import cdflynn.android.library.checkview.CheckView;

public class TenderPayActivity extends BaseActivity implements OnPaymentSelect {
    String selectedDeliveryType;
    String totalValue, totalStr,deliveryDateStr;
    TextView total, tax, totalWithTax,totolAddedAmount,changeToBeGiven;
    RecyclerView bill_rv;
    private List<SelectedMode> selectedModesList;
    private double totalWithTaxValue;
    LinearLayout pyment_ll,orderPlaced,digitalPayment_ll;
    Button track;
    CheckView check;
    RadioGroup deliveryType;
    String selectedPayment="Digital Payment";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tender_pay);
        tax = findViewById(R.id.textView13);
        total = findViewById(R.id.textView11);
        totalWithTax = findViewById(R.id.textView15);
        totolAddedAmount = findViewById(R.id.textView16);
        changeToBeGiven = findViewById(R.id.textView20);
        pyment_ll = findViewById(R.id.pyment_ll);
        orderPlaced = findViewById(R.id.orderPlaced);
        track = findViewById(R.id.track);
        check = findViewById(R.id.check);
        bill_rv = findViewById(R.id.bill_rv);
        deliveryType = findViewById(R.id.deliveryType);
        digitalPayment_ll = findViewById(R.id.linearLayout4);
        Intent intent = getIntent();
        selectedDeliveryType = intent.getStringExtra("selectedDeliveryType");
        deliveryDateStr = intent.getStringExtra("delivery_date");
        totalStr = intent.getStringExtra("total");
        total.setText("₹ " + totalStr.split("₹")[1]);

        if(selectedDeliveryType.equalsIgnoreCase("Store Pickup")){
            tax.setText("₹ " + roundOff(0.0));
        }else {
            tax.setText("₹ " + roundOff(Double.parseDouble(Preferences.getInstance(getApplicationContext()).getDelivery_charge())));
        }
        totalWithTaxValue = Double.parseDouble(totalStr.split("₹")[1]) + Double.parseDouble(tax.getText().toString().split("₹")[1]);
        totalValue = String.valueOf(totalWithTaxValue);
        totalWithTax.setText("₹ " + totalValue);
        selectedModesList = new ArrayList<>();

        deliveryType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                RadioButton rb= (RadioButton) findViewById(checkedId);
                selectedPayment = rb.getText().toString();
                if (selectedPayment.equals("Digital Payment")){
                    digitalPayment_ll.setVisibility(View.VISIBLE);
                }else {
                  digitalPayment_ll.setVisibility(View.GONE);
                }
            }
        });

        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.button3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog();
            }
        });



        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map request = new HashMap();
                Map voucher = new HashMap();
                if (selectedPayment.equals("Digital Payment")){
                    PaymentmodeList paymentmodeList = new PaymentmodeList(selectedModesList);
                    List<PaymentmodeList> paymentmodeLists = new ArrayList<>();
                    paymentmodeLists.add(paymentmodeList);
                    voucher.put("payment_settlement",paymentmodeLists);
                }
                request.put("voucher", voucher);
                voucher.put("payment_type", "");
                if(selectedDeliveryType.equalsIgnoreCase("Store Pickup")){
                    voucher.put("account_master_id", Preferences.getInstance(getApplicationContext()).getCashId());

                }else {
                    voucher.put("account_master_id", Preferences.getInstance(getApplicationContext()).getAddressId());
                }
                voucher.put("mobile_number", "");
                voucher.put("material_center_id", "");
                List<Data> list = AppUser.getAppUser(getApplicationContext()).getItemsData();
                List<Attributes> attributesList = new ArrayList<>();

                for (int i = 0; i < list.size(); i++) {
                    Attributes attr = list.get(i).getAttributes();
                    attr.setItem_id(attr.getId());
                    attr.setValue(attr.getQuantity() * attr.getSales_price_main());
                    attr.setTotal(attr.getItemTotal());
                    attr.setRate(attr.getSales_price_main());
                    attr.setDiscount(0.0);
                    attributesList.add(attr);
                }

                voucher.put("items", attributesList);
                voucher.put("deliveryType", selectedDeliveryType);
//                voucher.put("items", AppUser.getAppUser(getApplicationContext()).getItemsData());
                voucher.put("total", totalValue);
                voucher.put("gross_total", totalValue);
                voucher.put("payable_amount", totalValue);
                voucher.put("paid_amount", totalValue);
                voucher.put("previous_dues", "0.00");
                voucher.put("used_credits", "false");
                voucher.put("attachment", "");
                voucher.put("discount_val", "");
                voucher.put("without_tax", "false");
                voucher.put("user_id", Preferences.getInstance(getApplicationContext()).getUserId());
                voucher.put("mobile_company_user", Preferences.getInstance(getApplicationContext()).getUserId());
                voucher.put("user_name", Preferences.getInstance(getApplicationContext()).getName());
                voucher.put("billDiscount", "");
                voucher.put("couponDiscount", "");
                voucher.put("couponDiscountCode", "");
                voucher.put("points_used", "");
                voucher.put("points_amount", "");
                voucher.put("branch_id", AppUser.getAppUser(getApplicationContext()).getSignUpAttribute().getBranches().get(0).getId());
                voucher.put("delivery_exp_date",deliveryDateStr);
                voucher.put("delivery_exp_time", "");
                voucher.put("delivery_charge", tax.getText().toString());
                Log.d("DataVo", String.valueOf(voucher.get("payment_settlement")));

                apiCallBack(getApi().sale(ParameterConstant.COMPANY_ID, request));
            }
        });

        track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), OrderHistoryActivity.class));
                finish();
            }
        });


    }

    @Subscribe
    public void success(SuccessResponse response) {
        if (response.getStatus() == 200) {
            AppUser appUser = AppUser.getAppUser(getApplicationContext());
            appUser.setItemsData(new ArrayList<>());
            AppUser.saveAppUser(getApplicationContext(), appUser);
            orderPlaced.setVisibility(View.VISIBLE);
            pyment_ll.setVisibility(View.GONE);
            check.check();

        } else {

            getDialog(response.getMessage() + " " + response.getStatus());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        apiCallBackWithout(getApi().getDigitalPaymentList("1"));

    }

    @Subscribe
    public void getItemGroups(DigitalPaymentRespponse response) {
        if (response.getStatus() == 200) {
            MyProgressDialog.setDismiss();
            bill_rv.setHasFixedSize(true);
            bill_rv.setFocusable(false);
            bill_rv.setNestedScrollingEnabled(false);
            bill_rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
//            recyclerViewCategory.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
            bill_rv.setAdapter(new BillPaymentListAdapter(getApplicationContext(), response.getOrdered_accounts().get(0).getData(),TenderPayActivity.this));



        } else {
            getDialog(String.valueOf(response));
        }
    }

    @Override
    public void OnSelectedPayments(SelectedMode selectedMode,int position) {

        if(position<selectedModesList.size()){
            selectedModesList.set(position,selectedMode);
        }else {
            selectedModesList.add(selectedMode);
        }
        double countTotalSum=0.0;

        if(selectedModesList.size()>0){
            JSONArray payment_mode1 = new JSONArray();
            for(int j=0;j<selectedModesList.size();j++){
                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("amount",selectedModesList.get(j).getAmount());
                    jsonObject.put("payment_account_id",selectedModesList.get(j).getPayment_account_id());
                    jsonObject.put("voucher_type",selectedModesList.get(j).getVoucher_type());
                    jsonObject.put("payment_account_name",selectedModesList.get(j).getPayment_account_name());
                    payment_mode1.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                countTotalSum = countTotalSum+selectedModesList.get(j).getAmount();
                Log.d("Data....", String.valueOf(countTotalSum));

            }

        }
        totolAddedAmount.setText(String.valueOf(countTotalSum));
        changeToBeGiven.setText(String.valueOf((totalWithTaxValue-countTotalSum)));

    }

    public void getDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Remove Cart Item")
                .setMessage("Do you want to remove the items in your cart ?")
                .setCancelable(true)
                .setPositiveButton("Remove", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        AppUser appUser = AppUser.getAppUser(getApplicationContext());
                        appUser.setItemsData(new ArrayList<>());
                        AppUser.saveAppUser(getApplicationContext(), appUser);
                        Intent intent = new Intent(TenderPayActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                })

                .setNegativeButton("Cancel", null)
                .setIcon(R.drawable.storepe)
                .show();
    }

}
