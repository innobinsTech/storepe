package app.com.ecommerce.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.com.ecommerce.R;
import app.com.ecommerce.activity.AddAddressActivity;
import app.com.ecommerce.network.resposne.useraccounts.UserAccountData;
import app.com.ecommerce.utils.Preferences;

public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.ViewHolder>  {


    private Activity context;
    private List<UserAccountData> data;
    String intent;

    public AddressListAdapter(Activity context,String intent, List<UserAccountData> data) {
        this.context = context;
        this.intent = intent;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_address_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        //  viewHolder.title.setTypeface(roboto_regular);
        viewHolder.name.setText(data.get(position).getAttributes().getName().replace("|"," "));
        // viewHolder.tvHomeWork.setText(data.get(position).getMyOrderAttributes().getName());
        String completeAddress = "";
        if (data.get(position).getAttributes().getAddress()!=null){
            completeAddress = data.get(position).getAttributes().getAddress();
        } if (data.get(position).getAttributes().getCity()!=null){
            completeAddress = completeAddress +" "+ data.get(position).getAttributes().getCity();
        }if (data.get(position).getAttributes().getState()!=null){
            completeAddress = completeAddress +" "+ data.get(position).getAttributes().getState();
        }if (data.get(position).getAttributes().getPincode()!=null){
            completeAddress = completeAddress +" "+ data.get(position).getAttributes().getPincode();
        }
        viewHolder.tvAddress.setText(completeAddress.replace("|"," "));
        viewHolder.tvMobile.setText(data.get(position).getAttributes().getMobile_number());

        viewHolder.layoutEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddAddressActivity.class);
                intent.putExtra("id", data.get(position).getId());
                context.startActivity(intent);
            }
        });

        if (Preferences.getInstance(context).getAddressId().equals(data.get(position).getId())){
            viewHolder.radioButton.setChecked(true);
        }else {
            viewHolder.radioButton.setChecked(false);
        }

        String finalCompleteAddress = completeAddress;
        viewHolder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.getInstance(context).setAddressId(data.get(position).getId());
                Preferences.getInstance(context).setUserAddressName(viewHolder.name.getText().toString());
                Preferences.getInstance(context).setUserAddress(finalCompleteAddress);
                Preferences.getInstance(context).setUserAddressMobile(data.get(position).getAttributes().getMobile_number());
                notifyDataSetChanged();
            }
        });

        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.getInstance(context).setAddressId(data.get(position).getId());
                Preferences.getInstance(context).setUserAddressName(viewHolder.name.getText().toString());
                Preferences.getInstance(context).setUserAddress(finalCompleteAddress.replace("|"," "));
                Preferences.getInstance(context).setUserAddressMobile(data.get(position).getAttributes().getMobile_number());
                notifyDataSetChanged();
                if (intent!=null){
                    context.finish();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, tvHomeWork, tvAddress, tvMobile;
        LinearLayout layoutEdit;
        LinearLayout mainLayout;
        RadioButton radioButton;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            tvHomeWork = itemView.findViewById(R.id.tvHomeWork);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            tvMobile = itemView.findViewById(R.id.tvMobile);
            layoutEdit = itemView.findViewById(R.id.layoutEdit);
            radioButton = itemView.findViewById(R.id.radioButton);
            mainLayout = itemView.findViewById(R.id.mainLayout);
        }
    }


}