package app.com.ecommerce.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import app.com.ecommerce.R;
import app.com.ecommerce.activity.base.BaseActivity;
import app.com.ecommerce.network.Purzi;
import app.com.ecommerce.network.request.ItemPaginationRequest;
import app.com.ecommerce.network.resposne.itemgroups.Data;
import app.com.ecommerce.utils.MyProgressDialog;
import app.com.ecommerce.utils.ParameterConstant;
import app.com.ecommerce.utils.Preferences;
import butterknife.BindView;

public class PurziAdapter extends RecyclerView.Adapter<PurziAdapter.ViewHolder> {

    private Activity context;
    private List<Purzi> purziList;
    List<TextView> viewList;


    public PurziAdapter(Activity context, List<Purzi> data,List<TextView> viewList) {
        this.context = context;
        this.purziList = data;
        this.viewList = viewList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_purzi, viewGroup, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        viewHolder.name.setText(purziList.get(position).getName());
        viewHolder.brand.setText(purziList.get(position).getBrand());
        viewHolder.unit.setText(purziList.get(position).getUnit());
        viewHolder.description.setText(purziList.get(position).getDescription());
        viewHolder.quantity.setText(purziList.get(position).getQuantity());
        viewHolder.position.setText(""+(position+1));
        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(context)
                        .setTitle("Delete")
                        .setMessage("Are you sure you want to delete?")
                        .setCancelable(true)
                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                purziList.remove(position);
                                notifyDataSetChanged();
                                for (int i=0;i<viewList.size();i++){
                                    viewList.get(i).clearFocus();
                                }

                            }
                        })

                        .setNegativeButton("Cancel", null)
                        .setIcon(R.drawable.storepe)
                        .show();

            }
        });

        viewHolder.quantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogQuantity(purziList.get(position));
            }
        });
    }


    @Override
    public int getItemCount() {
        return purziList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView brand;
        TextView description;
        TextView unit;
        TextView quantity;
        TextView position;
        LinearLayout delete;

        public ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            brand = itemView.findViewById(R.id.brand);
            description = itemView.findViewById(R.id.description);
            unit = itemView.findViewById(R.id.unit);
            quantity = itemView.findViewById(R.id.quantity);
            delete = itemView.findViewById(R.id.delete);
            position = itemView.findViewById(R.id.position);
        }
    }


    void dialogQuantity(Purzi purzi) {
        final BottomSheetDialog d  = new BottomSheetDialog(context);
        Objects.requireNonNull(d.getWindow()).setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        d.setContentView(R.layout.dialog_quantity);
        d.getBehavior().setHideable(false);
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        EditText editText=d.findViewById(R.id.editText);
        TextView submit=d.findViewById(R.id.submit);
        editText.requestFocus();
        editText.setText(purzi.getQuantity());
        editText.setSelection(editText.getText().toString().length());
        if (editText.getText().toString().isEmpty()){
            submit.setTextColor(context.getResources().getColor(R.color.default_text));
        }else {
            submit.setTextColor(context.getResources().getColor(R.color.blue));
        }

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editText.getText().toString().isEmpty()){
                    submit.setTextColor(context.getResources().getColor(R.color.default_text));
                }else {
                    submit.setTextColor(context.getResources().getColor(R.color.blue));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText().toString().isEmpty()){
                    Toast.makeText(context, "Enter quantity", Toast.LENGTH_SHORT).show();
                    return;
                }
                purzi.setQuantity(editText.getText().toString());
                d.dismiss();
                notifyDataSetChanged();
            }
        });
        d.show();
    }

}