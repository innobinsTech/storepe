package app.com.ecommerce.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.google.android.material.tabs.TabItem;
import com.orhanobut.dialogplus.DialogPlus;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;
import java.util.ArrayList;

import app.com.ecommerce.R;
import app.com.ecommerce.activity.HomeActivity;
import app.com.ecommerce.activity.base.BaseActivity;
import app.com.ecommerce.network.request.ItemPaginationRequest;
import app.com.ecommerce.network.resposne.itemgroups.Data;
import app.com.ecommerce.utils.MyProgressDialog;
import app.com.ecommerce.utils.ParameterConstant;
import app.com.ecommerce.utils.Preferences;

public class ItemGroupListAdapter extends RecyclerView.Adapter<ItemGroupListAdapter.ViewHolder> {

    private Activity context;
    private ArrayList<Data> data;
    private static DecimalFormat df2 = new DecimalFormat("#.##");

    public ItemGroupListAdapter(Activity context,ArrayList<Data> data) {
        this.context = context;
        this.data = data;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_category, viewGroup, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.name.setText(data.get(position).getAttributes().getName());
        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.pageNo=1;
                ItemPaginationRequest request = new ItemPaginationRequest();
                request.setCompany_id(ParameterConstant.COMPANY_ID);
                request.setLimit("" + "10");
                request.setPage_no("" + "1");
                request.setUser_id(Preferences.getInstance(context).getUserId());
                request.setItem_group_id(data.get(position).getId());
                MyProgressDialog.getInstance(context).show();
                BaseActivity.context.apiCallBackWithout(BaseActivity.context.getApi().getItemsGroupBy(request));
//                ApiCallService.action(getActivity(), request, ACTION_GET_ITEM_GROUPS_BY);
            }
        });

        Glide.with(context)
                .load(ParameterConstant.BASE_IMAGE_URL+""+data.get(position).getAttributes().getItem_group_image_cover_url())
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(viewHolder.image);


    }


    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView image;
        RelativeLayout mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            mainLayout = itemView.findViewById(R.id.mainLayout);
            image = itemView.findViewById(R.id.image);
        }
    }
}