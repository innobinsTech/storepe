package app.com.ecommerce.adapter;

import android.app.Activity;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.com.ecommerce.R;
import app.com.ecommerce.network.resposne.inv.Data;
import app.com.ecommerce.network.resposne.inv.OrderedItem;
import app.com.ecommerce.utils.Preferences;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OnGoingItemAdapter extends RecyclerView.Adapter<OnGoingItemAdapter.ViewHolder> {

    Activity context;
    public static List<OrderedItem> responseList;
    public static List<OrderedItem> filteredList;
    boolean isChecked;

    public OnGoingItemAdapter(Activity context, List<OrderedItem> data) {
        this.context = context;
        this.responseList = data;
        this.filteredList = data;
        this.isChecked = isChecked;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_history, viewGroup, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.name.setText(responseList.get(position).getName());
        viewHolder.price.setText("" + responseList.get(position).getSales_price_main());
        viewHolder.quantity.setText("" + responseList.get(position).getQuantity());
        viewHolder.amount.setText("" + responseList.get(position).getTotal_amount());
        viewHolder.tax.setText("" + responseList.get(position).getTax_category_name());

    }


    @Override
    public int getItemCount() {
        return responseList.size();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.amount)
        TextView amount;
        @BindView(R.id.quantity)
        TextView quantity;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.tax)
        TextView tax;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}