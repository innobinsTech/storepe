package app.com.ecommerce.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import app.com.ecommerce.R;
import app.com.ecommerce.network.resposne.item.Data;
import app.com.ecommerce.utils.EventClick;
import app.com.ecommerce.utils.ParameterConstant;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> implements Filterable {

    private Context context;
    Double sale_price_main = 0.0;
    Double mrp_price = 0.0;
    Double percentage = 0.0;
    List<Data> dataList;
   static List<Data> filteredDataList;
    private Dialog dialog;

    public CartAdapter(Context context, List<Data> data) {
        this.context = context;
        this.dataList = data;
        this.filteredDataList = data;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_home, viewGroup, false);
        return new ViewHolder(view);
    }

    int mInteger = 0;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        mInteger = 0;
        String name, item_id, quantity, itemTax, detail;

        if (dataList.get(position).getAttributes().getPackaging_categories().size() > 0) {
            int pos = -1;
            if (dataList.get(position).getPackaging_category_id()!=null){
               /* if (dataList.get(position).getPackaging_category_id().equals("54")){
                    System.out.println();
                }*/
                for (int i=0;i<dataList.get(position).getAttributes().getPackaging_categories().size();i++){
                    if (dataList.get(position).getPackaging_category_id().equalsIgnoreCase(String.valueOf(dataList.get(position).getAttributes().getPackaging_categories().get(i).getPackaging_category_id()))){
                     pos = i;
                     break;
                    }else {
                        pos = dataList.get(position).getAttributes().getPackaging_categories().size() - 1;
                    }
                }
            }else {
                pos = dataList.get(position).getAttributes().getPackaging_categories().size() - 1;
            }
            viewHolder.selectPackaging.setVisibility(View.VISIBLE);
            name = dataList.get(position).getAttributes().getPackaging_categories().get(pos).getName();
            quantity = "" + dataList.get(position).getAttributes().getTotal_stock_quantity();
            item_id = "" + dataList.get(position).getAttributes().getPackaging_categories().get(pos).getItem_id();
            itemTax = dataList.get(position).getAttributes().getTax_category();
            detail = dataList.get(position).getAttributes().getPackaging_categories().get(pos).getDescription();
            sale_price_main = 0.0;
            mrp_price = 0.0;
            if (!String.valueOf(dataList.get(position).getAttributes().getPackaging_categories().get(pos).getSales_price()).equals("null")
                    && !String.valueOf(dataList.get(position).getAttributes().getPackaging_categories().get(pos).getSales_price()).equals("")) {
                sale_price_main = dataList.get(position).getAttributes().getPackaging_categories().get(pos).getSales_price();
            } else {
                sale_price_main = 0.0;
            }
            if (!String.valueOf(dataList.get(position).getAttributes().getPackaging_categories().get(pos).getMrp()).equals("null")
                    && !String.valueOf(dataList.get(position).getAttributes().getPackaging_categories().get(pos).getMrp()).equals("")) {
                mrp_price = dataList.get(position).getAttributes().getPackaging_categories().get(pos).getMrp();
            } else {
                mrp_price = 0.0;
            }
            if (dataList.get(position).getAttributes().getPackaging_categories().get(pos).getItem_image().getThumb().getUrl() != null && !dataList.get(position).getAttributes().getPackaging_categories().get(pos).getItem_image().getThumb().getUrl().equals("")) {
                viewHolder.mImageView.setVisibility(View.VISIBLE);
                Glide.with(context)
                        .load(ParameterConstant.BASE_URL + dataList.get(position).getAttributes().getPackaging_categories().get(pos).getItem_image().getThumb().getUrl())
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(viewHolder.mImageView);
            } else {
                viewHolder.mImageView.setVisibility(View.VISIBLE);
                viewHolder.mImageView.setImageDrawable(context.getResources().getDrawable(R.drawable.default_image));
            }
            viewHolder.name.setText(name);
            viewHolder.mDetail.setText(detail);
            String arr[] = dataList.get(position).getAttributes().getPackaging_categories().get(pos).getName().split("-");
            if (arr.length == 1) {
                viewHolder.packagingName.setText(dataList.get(position).getAttributes().getPackaging_categories().get(pos).getName());
            } else {
                viewHolder.packagingName.setText(dataList.get(position).getAttributes().getPackaging_categories().get(pos).getName().split("-")[1]);
            }
            percentage = 0.0;
            if (sale_price_main != 0 && mrp_price != 0) {
//                viewHolder.mItemPrice.setVisibility(View.VISIBLE);
                viewHolder.mrp.setVisibility(View.VISIBLE);
                viewHolder.layoutPercentage.setVisibility(View.VISIBLE);
                percentage = ((mrp_price - sale_price_main) * 100) / mrp_price;
//                viewHolder.mItemPrice.setText("₹ " + sale_price_main);
                viewHolder.mrp.setText("₹ " + mrp_price);
                String per = String.valueOf(percentage);
                viewHolder.percentage.setText(per.split("\\.")[0] + "% off");
                viewHolder.mrp.setPaintFlags(viewHolder.mrp.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                if (sale_price_main != 0) {
//                    viewHolder.mItemPrice.setVisibility(View.VISIBLE);
                    viewHolder.mrp.setVisibility(View.GONE);
                    viewHolder.layoutPercentage.setVisibility(View.GONE);
//                    viewHolder.mItemPrice.setText("₹ " + sale_price_main);
                } else if (mrp_price != 0) {
//                    viewHolder.mItemPrice.setVisibility(View.GONE);
                    viewHolder.mrp.setVisibility(View.VISIBLE);
                    viewHolder.layoutPercentage.setVisibility(View.GONE);
                    String mrp = "₹ " + "<b>" + mrp_price + "</b>";
                    viewHolder.mrp.setText(Html.fromHtml(mrp));
                    viewHolder.mrp.setTextColor(context.getResources().getColor(R.color.black));
                } else {
//                    viewHolder.mItemPrice.setVisibility(View.GONE);
                    viewHolder.mrp.setVisibility(View.GONE);
                    viewHolder.layoutPercentage.setVisibility(View.GONE);
                }
            }
        }
        else {
            viewHolder.selectPackaging.setVisibility(View.GONE);
            name = dataList.get(position).getAttributes().getName();
            quantity = "" + dataList.get(position).getAttributes().getTotal_stock_quantity();
            item_id = dataList.get(position).getAttributes().getId();
            itemTax = dataList.get(position).getAttributes().getTax_category();
            detail = dataList.get(position).getAttributes().getItem_description();
            sale_price_main = 0.0;
            mrp_price = 0.0;
            if ("" + dataList.get(position).getAttributes().getSales_price_main() != null
                    && !String.valueOf(dataList.get(position).getAttributes().getSales_price_main()).equals("")) {
                sale_price_main = dataList.get(position).getAttributes().getSales_price_main();
            } else {
                sale_price_main = 0.0;
            }
            if ("" + dataList.get(position).getAttributes().getMrp() != null
                    && !String.valueOf(dataList.get(position).getAttributes().getMrp()).equals("")) {
                mrp_price = dataList.get(position).getAttributes().getMrp();
            } else {
                mrp_price = 0.0;
            }
            if (dataList.get(position).getAttributes().getItem_image_thumb_url() != null && !dataList.get(position).getAttributes().getItem_image_thumb_url().equals("")) {
                viewHolder.mImageView.setVisibility(View.VISIBLE);
                Glide.with(context)
                        .load(ParameterConstant.BASE_URL + dataList.get(position).getAttributes().getItem_image_thumb_url())
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(viewHolder.mImageView);
            } else {
                viewHolder.mImageView.setVisibility(View.VISIBLE);
                viewHolder.mImageView.setImageDrawable(context.getResources().getDrawable(R.drawable.default_image));
            }
            viewHolder.name.setText(name);
            viewHolder.mDetail.setText(detail);
            percentage = 0.0;
            if (sale_price_main != 0 && mrp_price != 0) {
//                viewHolder.mItemPrice.setVisibility(View.VISIBLE);
                viewHolder.mrp.setVisibility(View.VISIBLE);
                viewHolder.layoutPercentage.setVisibility(View.VISIBLE);
                percentage = ((mrp_price - sale_price_main) * 100) / mrp_price;
//                viewHolder.mItemPrice.setText("₹ " + sale_price_main);
                viewHolder.mrp.setText("₹ " + mrp_price);
                String per = String.valueOf(percentage);
                viewHolder.percentage.setText(per.split("\\.")[0] + "% off");
                viewHolder.mrp.setPaintFlags(viewHolder.mrp.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                if (sale_price_main != 0) {
//                    viewHolder.mItemPrice.setVisibility(View.VISIBLE);
                    viewHolder.mrp.setVisibility(View.GONE);
                    viewHolder.layoutPercentage.setVisibility(View.GONE);
//                    viewHolder.mItemPrice.setText("₹ " + sale_price_main);
                } else if (mrp_price != 0) {
//                    viewHolder.mItemPrice.setVisibility(View.GONE);
                    viewHolder.mrp.setVisibility(View.VISIBLE);
                    viewHolder.layoutPercentage.setVisibility(View.GONE);
                    String mrp = "₹ " + "<b>" + mrp_price + "</b>";
                    viewHolder.mrp.setText(Html.fromHtml(mrp));
                    viewHolder.mrp.setTextColor(context.getResources().getColor(R.color.black));
                } else {
//                    viewHolder.mItemPrice.setVisibility(View.GONE);
                    viewHolder.mrp.setVisibility(View.GONE);
                    viewHolder.layoutPercentage.setVisibility(View.GONE);
                }
            }
        }

        if (dataList.get(position).getAttributes().getQuantity()==0){
            viewHolder.add.setVisibility(View.VISIBLE);
            viewHolder.minus.setVisibility(View.GONE);
            viewHolder.quantity.setVisibility(View.GONE);
        }else {
            viewHolder.add.setVisibility(View.GONE);
            viewHolder.minus.setVisibility(View.VISIBLE);
            viewHolder.quantity.setVisibility(View.VISIBLE);
        }

        if (dataList.get(position).getAttributes().isProgress()){
            viewHolder.progressBar.setVisibility(View.VISIBLE);
        }else {
            viewHolder.progressBar.setVisibility(View.GONE);
        }

        viewHolder.quantity.setText(""+dataList.get(position).getAttributes().getQuantity());
        viewHolder.mItemAmount.setText("₹ " + dataList.get(position).getAttributes().getItemTotal());
        viewHolder.mItemPrice.setText("₹ " + roundOff(dataList.get(position).getAttributes().getSales_price_inclusive()));

        viewHolder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataList.get(position).getAttributes().setProgress(true);
                EventBus.getDefault().post(new EventClick(position,true));
                notifyDataSetChanged();
            }
        });


        viewHolder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataList.get(position).getAttributes().setProgress(true);
                EventBus.getDefault().post(new EventClick(position,true));
                notifyDataSetChanged();
            }
        });



        viewHolder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataList.get(position).getAttributes().setProgress(true);
                EventBus.getDefault().post(new EventClick(position,false));
                notifyDataSetChanged();
            }
        });




    }





    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mainLayout,  layoutPercentage, selectPackaging;;
        TextView name,  mItemPrice, mItemAmount, mDetail, mrp, percentage, packagingName;
        ImageView mImageView, downArrow;
        TextView add,plus,minus,quantity;
        ProgressBar progressBar;

        public ViewHolder(View itemView) {
            super(itemView);
            mainLayout = itemView.findViewById(R.id.main_layout);
            name = itemView.findViewById(R.id.name);
            mItemPrice = itemView.findViewById(R.id.price);
            mItemAmount = itemView.findViewById(R.id.itemTotal);
            mDetail = itemView.findViewById(R.id.detail);
            mImageView = itemView.findViewById(R.id.mImageView);
            mrp = itemView.findViewById(R.id.mrp);
            percentage = itemView.findViewById(R.id.percentage);
            layoutPercentage = itemView.findViewById(R.id.layoutPercentage);
            selectPackaging = itemView.findViewById(R.id.selectPackaging);
            downArrow = itemView.findViewById(R.id.downArrow);
            packagingName = itemView.findViewById(R.id.packagingName);
            add = itemView.findViewById(R.id.add);
            plus = itemView.findViewById(R.id.plus);
            minus = itemView.findViewById(R.id.minus);
            quantity = itemView.findViewById(R.id.quantity);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredDataList = dataList;
                } else {
                    ArrayList<Data> filteredDataList = new ArrayList<>();
                    for (Data branchResponse : dataList) {
                        if (branchResponse.getAttributes().getName().toLowerCase().startsWith(charString) /*|| androidVersion.getMobile().startsWith(charString)*/) {
                            filteredDataList.add(branchResponse);
                        } /*else if (branchResponse.getLname().toLowerCase().contains(charString) *//*|| androidVersion.getMobile().contains(charString)*//*) {
                            filteredDataList.add(branchResponse);
                        } else if (branchResponse.getMobile().contains(charString) *//*|| androidVersion.getMobile().contains(charString)*//*) {
                            filteredDataList.add(branchResponse);
                        }*/
                    }
                    CartAdapter.filteredDataList = filteredDataList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredDataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredDataList = (ArrayList<Data>) filterResults.values;
                dataList = filteredDataList;
                notifyDataSetChanged();
            }
        };
    }



    public String roundOff(double val) {
        return String.format("%.1f", val);
    }




}