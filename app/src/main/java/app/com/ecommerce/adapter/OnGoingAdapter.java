package app.com.ecommerce.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.com.ecommerce.R;
import app.com.ecommerce.activity.PrintActivity;
import app.com.ecommerce.activity.base.BaseActivity;
import app.com.ecommerce.events.CancelEvent;
import app.com.ecommerce.network.resposne.inv.Data;
import app.com.ecommerce.utils.Preferences;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OnGoingAdapter extends RecyclerView.Adapter<OnGoingAdapter.ViewHolder> implements Filterable {

    Activity context;
    public static List<Data> responseList;
    public static List<Data> filteredList;
    boolean isChecked;

    public OnGoingAdapter(Activity context, List<Data> data) {
        this.context = context;
        this.responseList = data;
        this.filteredList = data;
        this.isChecked = isChecked;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_on_going, viewGroup, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Data data = responseList.get(position);

        viewHolder.name.setText(Preferences.getInstance(context).getName());
        if (data.getAttributes().getDeliveryType().contains("Store")) {
            viewHolder.address.setText(data.getAttributes().getDeliveryType());
        } else {
            viewHolder.address.setText(data.getAttributes().getAccount_master().replace("|", " "));
        }
        viewHolder.amount.setText("₹" + data.getAttributes().getTotal_amount());
        viewHolder.orderId.setText(data.getAttributes().getVoucher_series());
        viewHolder.status.setText(data.getAttributes().getStatus_name());
        viewHolder.time.setText(data.getAttributes().getDate() + " " + data.getAttributes().getTime());

        viewHolder.recyclerView.setHasFixedSize(true);
        viewHolder.recyclerView.setFocusable(false);
        viewHolder.recyclerView.setNestedScrollingEnabled(true);
        viewHolder.recyclerView.setLayoutManager(new GridLayoutManager(context, 1));
        viewHolder.recyclerView.setAdapter(new OnGoingItemAdapter(context, data.getAttributes().getVoucher_items()));

        viewHolder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new CancelEvent(position));
            }
        });

        viewHolder.a4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, PrintActivity.class);
                intent.putExtra("company_report",data.getAttributes().getInvoice_html());
                intent.putExtra("three",data.getAttributes().getThree_inch_invoice());
                intent.putExtra("four",data.getAttributes().getFour_inch_invoice());
                intent.putExtra("mobile",data.getAttributes().getMobile_number());
                intent.putExtra("id",""+data.getAttributes().getId());
                context.startActivity(intent);
        }
        });


        viewHolder.inch3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, PrintActivity.class);
                intent.putExtra("company_report",data.getAttributes().getThree_inch_invoice());
                intent.putExtra("mobile",data.getAttributes().getMobile_number());
                intent.putExtra("id",""+data.getAttributes().getId());
                context.startActivity(intent);
            }
        });


        viewHolder.inch4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, PrintActivity.class);
                intent.putExtra("company_report",data.getAttributes().getFour_inch_invoice());
                intent.putExtra("mobile",data.getAttributes().getMobile_number());
                intent.putExtra("id",""+data.getAttributes().getId());
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return responseList.size();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.amount)
        TextView amount;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.orderId)
        TextView orderId;
        @BindView(R.id.recyclerView)
        RecyclerView recyclerView;
        @BindView(R.id.cancel)
        LinearLayout cancel;
        @BindView(R.id.a4)
        LinearLayout a4;
        @BindView(R.id.inch3)
        LinearLayout inch3;
        @BindView(R.id.inch4)
        LinearLayout inch4;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString().toLowerCase();
                if (charString.isEmpty()) {
                    filteredList = responseList;
                } else {
                    ArrayList<Data> filteredList = new ArrayList<>();
                    for (Data data : responseList) {
                        if (data.getAttributes().getAccount_master().toLowerCase().toLowerCase().startsWith(charString) || data.getAttributes().getAccount_master().toLowerCase().contains(charString)) {
                            filteredList.add(data);
                        }
                        if (data.getAttributes().getVoucher_series().toLowerCase().toLowerCase().startsWith(charString) || data.getAttributes().getVoucher_series().toLowerCase().contains(charString)) {
                            filteredList.add(data);
                        }
                    }
                    OnGoingAdapter.filteredList = filteredList;
                }
                System.out.println("http result " + filteredList);
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (ArrayList<Data>) filterResults.values;
                responseList = filteredList;
                notifyDataSetChanged();
            }
        };
    }


}