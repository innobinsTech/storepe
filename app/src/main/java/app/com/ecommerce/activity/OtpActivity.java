package app.com.ecommerce.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;

import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.Map;

import app.com.ecommerce.R;
import app.com.ecommerce.activity.base.BaseActivity;
import app.com.ecommerce.activity.location_picker.PlacePickerActivity;
import app.com.ecommerce.network.resposne.signup.ResendOtpResponse;
import app.com.ecommerce.network.resposne.signup.SignUpResponse;
import app.com.ecommerce.utils.ParameterConstant;
import app.com.ecommerce.utils.Preferences;

public class OtpActivity extends BaseActivity {
    Button otp;
    PinView pinView;
    RelativeLayout resendOtpLayout;
    TextView mobile;
    TextView timer;
    String mMobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        findIds();
        Intent intent = getIntent();
        mMobile = intent.getStringExtra("mobile");
        mobile.setText("+91 "+mMobile);
        otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinView.getText().toString().length()==0){
                    getDialog("Oops!","Enter Otp");
                    return;
                }
                if (pinView.getText().toString().length()!=4){
                    getDialog("Oops!","Enter complete OTP");
                    return;
                }
                Map map = new HashMap();
                Map map1 = new HashMap();
                map.put("mobile",mMobile);
                map.put("otp",pinView.getText().toString());
                map.put("company_id", ParameterConstant.COMPANY_ID);
                map.put("client_token","");
                map1.put("user",map);
                apiCallBack(getApi().otp(map1));
            }
        });

        resendOtpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map map = new HashMap();
                Map map1 = new HashMap();
                map.put("mobile",mMobile);
                map1.put("user",map);
                apiCallBack(getApi().resendOtp(map1));
            }
        });


    }


    private void findIds() {
        otp=findViewById(R.id.login);
        mobile=findViewById(R.id.mobile);
        pinView=findViewById(R.id.pinView);
        timer=findViewById(R.id.timer);
        resendOtpLayout=findViewById(R.id.resendOtpLayout);
    }
    @Subscribe
    public void response(SignUpResponse response){
        if (response.getStatus()==200){
            if (response.getMessage()==null){
                Preferences.getInstance(OtpActivity.this).setLogin(true);
                Preferences.getInstance(OtpActivity.this).setToken(response.getUser().getData().getAttributes().getAuth_token());
                Preferences.getInstance(OtpActivity.this).setUserId(response.getUser().getData().getId());
                Preferences.getInstance(OtpActivity.this).setName(response.getUser().getData().getAttributes().getName());
                Preferences.getInstance(OtpActivity.this).setMobile(response.getUser().getData().getAttributes().getMobile());
                Intent intent = new Intent(OtpActivity.this, HomeActivity.class);
//                Intent intent = new Intent(OtpActivity.this, PlacePickerActivity.class);
                intent.putExtra("setup","yes");
                startActivity(intent);
            }else {
                getDialog(response.getMessage());
            }
        }else {
            getDialog(response.getMessage());
        }
    }

    @Subscribe
    public void response(ResendOtpResponse response){
        if (response.getStatus()==200){
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }else {
             getDialog("Sorry",response.getMessage());
        }

    }
}
