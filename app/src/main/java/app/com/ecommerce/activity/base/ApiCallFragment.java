package app.com.ecommerce.activity.base;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import app.com.ecommerce.R;
import app.com.ecommerce.network.Api;
import app.com.ecommerce.network.ThisApp;
import app.com.ecommerce.utils.Helper;
import app.com.ecommerce.utils.MyProgressDialog;
import app.com.ecommerce.utils.ParameterConstant;
import me.ibrahimsn.lib.CirclesLoadingView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ApiCallFragment<T> extends Fragment {

    Call<T> callback;
    Api api;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        api = ThisApp.getApi(getActivity());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }


    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe
    public void timeOut(String msg) {
        getDialogFailed(msg);
    }


    public Api getApi() {
        return ThisApp.getApi(getActivity());
    }

    public void apiCallBack(final Call<T> callback) {
        this.callback = callback;
        if (!Helper.isNetworkAvailable(getContext())) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(ParameterConstant.NO_INTERNET_TITTLE)
                    .setMessage(ParameterConstant.NO_INTERNET_MESSAGE)
                    .setCancelable(false)
                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            apiCallBack(callback);
                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .setIcon(R.mipmap.ic_launcher)
                    .show();

        } else {
            callback.enqueue(new Local<T>());
        }

    }


    public void apiCallBack(CirclesLoadingView circlesLoadingView,final Call<T> callback) {
        this.callback = callback;
        if (!Helper.isNetworkAvailable(getContext())) {
            circlesLoadingView.setVisibility(View.GONE);
            new AlertDialog.Builder(getActivity())
                    .setTitle(ParameterConstant.NO_INTERNET_TITTLE)
                    .setMessage(ParameterConstant.NO_INTERNET_MESSAGE)
                    .setCancelable(false)
                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            apiCallBack(circlesLoadingView,callback);
                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .setIcon(R.mipmap.ic_launcher)
                    .show();

        } else {
            circlesLoadingView.setVisibility(View.VISIBLE);
            callback.enqueue(new Local<T>());
        }

    }


    public void getDialogFailed(String message) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Sorry")
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })

//                .setNegativeButton("Cancel", null)
                .setIcon(R.mipmap.ic_launcher)
                .show();
    }

  public   void getDialog(String tittle, String message) {
        new AlertDialog.Builder(getActivity())
                .setTitle(tittle)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        apiCallBack(callback);
                    }
                })

//                .setNegativeButton("Exit", null)
                .setIcon(R.mipmap.ic_launcher)
                .show();
    }


    class Local<T> implements Callback<T> {

        @Override
        public void onResponse(Call<T> call, Response<T> response) {
            MyProgressDialog.setDismiss();
            if (response.code() == 200) {
                T body = response.body();
                EventBus.getDefault().post(body);
            } else {
                EventBus.getDefault().post(ParameterConstant.ERROR + " " + response.code());
            }
        }

        @Override
        public void onFailure(Call<T> call, Throwable t) {
            MyProgressDialog.setDismiss();
            EventBus.getDefault().post(t.getMessage());
        }
    }



}
