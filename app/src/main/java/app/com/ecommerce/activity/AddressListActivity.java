package app.com.ecommerce.activity;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import app.com.ecommerce.R;
import app.com.ecommerce.activity.base.BaseActivity;
import app.com.ecommerce.adapter.AddressListAdapter;
import app.com.ecommerce.network.resposne.useraccounts.UserAccountData;
import app.com.ecommerce.network.resposne.useraccounts.UserAccountRespponse;
import app.com.ecommerce.utils.Preferences;

public class AddressListActivity extends BaseActivity {

    private RecyclerView mRecyclerView;
    private LinearLayout layoutAddNewAddress;
    String intent;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);
        mRecyclerView = findViewById(R.id.recyclerView);
        layoutAddNewAddress = findViewById(R.id.layoutAddNewAddress);
        initActionbar("Address");
        intent=getIntent().getStringExtra("intent");

        layoutAddNewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddressListActivity.this, AddAddressActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
       apiCallBack(getApi().getAccountsList(Preferences.getInstance(this).getUserId()));
        super.onResume();
    }



    private void setAdapter(ArrayList<UserAccountData> accountsArrayList) {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setFocusable(false);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(new AddressListAdapter(this,intent, accountsArrayList));
    }

    @Subscribe
    public void getUserAccounts(UserAccountRespponse response) {
        if (response.getStatus() == 200) {
            ArrayList<UserAccountData> accountsArrayList = new ArrayList<>();
            for (int i = 0; i < response.getOrdered_accounts().size(); i++) {
                UserAccountData data = new UserAccountData();
                for (int j = 0; j < response.getOrdered_accounts().get(i).getData().size(); j++) {
                    accountsArrayList.add(response.getOrdered_accounts().get(i).getData().get(j));
                }
            }
            setAdapter(accountsArrayList);
        } else {
            getDialog( response.getMessage());
        }
    }


}
