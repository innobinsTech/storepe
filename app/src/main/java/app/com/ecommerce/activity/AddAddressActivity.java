package app.com.ecommerce.activity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.Map;

import app.com.ecommerce.R;
import app.com.ecommerce.activity.base.BaseActivity;
import app.com.ecommerce.network.request.CreateAccount;
import app.com.ecommerce.network.request.CreateAccountRequest;
import app.com.ecommerce.network.resposne.SuccessResponse;
import app.com.ecommerce.network.resposne.useraccounts.AccountDetailResponse;
import app.com.ecommerce.network.resposne.useraccounts.CreateAccountResponse;
import app.com.ecommerce.network.resposne.useraccounts.UserAccountAttributes;
import app.com.ecommerce.utils.ParameterConstant;
import app.com.ecommerce.utils.Preferences;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AddAddressActivity extends BaseActivity {

    @BindView(R.id.firstName)
    TextInputEditText firstName;
    @BindView(R.id.firstNameLayout)
    TextInputLayout firstNameLayout;
    @BindView(R.id.lastName)
    TextInputEditText lastName;
    @BindView(R.id.lastNameLayout)
    TextInputLayout lastNameLayout;
    @BindView(R.id.mobile)
    TextInputEditText mobile;
    @BindView(R.id.mobileLayout)
    TextInputLayout mobileLayout;
    @BindView(R.id.houseNo)
    TextInputEditText houseNo;
    @BindView(R.id.houseNoLayout)
    TextInputLayout houseNoLayout;
    @BindView(R.id.apartment)
    TextInputEditText apartment;
    @BindView(R.id.apartmentLayout)
    TextInputLayout apartmentLayout;
    @BindView(R.id.street)
    TextInputEditText street;
    @BindView(R.id.landmark)
    TextInputEditText landmark;
    @BindView(R.id.area)
    TextInputEditText area;
    @BindView(R.id.state)
    Spinner state;
    @BindView(R.id.pin)
    TextInputEditText pin;
    @BindView(R.id.home)
    TextView home;
    @BindView(R.id.other)
    TextView other;
    @BindView(R.id.otherInput)
    TextInputEditText otherInput;
    @BindView(R.id.otherLayout)
    TextInputLayout otherLayout;
    @BindView(R.id.submit)
    LinearLayout submit;
    @BindView(R.id.areaLayout)
    TextInputLayout areaLayout;
    @BindView(R.id.pinLayout)
    TextInputLayout pinLayout;
    @BindView(R.id.office)
    TextView office;
    String id;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_adress);
        ButterKnife.bind(this);
        id = getIntent().getStringExtra("id");
        if (id == null) {
            initActionbar("Add Address");
        } else {
            initActionbar("Update Address");
            apiCallBack(getApi().getAccountDetails(id));
        }

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean validation = true;
                if (firstName.getText().toString().trim().isEmpty()) {
                    firstNameLayout.setError("Enter first name");
                    validation = false;
                }

                if (lastName.getText().toString().trim().isEmpty()) {
                    lastNameLayout.setError("Enter last name");
                    validation = false;
                }

                if (mobile.getText().toString().trim().isEmpty()) {
                    mobileLayout.setError("Enter contact number");
                    validation = false;
                }

                if (mobile.getText().toString().trim().length() != 10) {
                    mobileLayout.setError("Enter valid contact number");
                    validation = false;
                }

                if (houseNo.getText().toString().trim().isEmpty()) {
                    houseNoLayout.setError("Enter house number");
                    validation = false;
                }


                if (apartment.getText().toString().trim().isEmpty()) {
                    apartmentLayout.setError("Enter apartment name");
                    validation = false;
                }
                if (area.getText().toString().trim().isEmpty()) {
                    areaLayout.setError("Enter area");
                    validation = false;
                }

                if (pin.getText().toString().trim().isEmpty()) {
                    pinLayout.setError("Enter pincode");
                    validation = false;
                }

                if (pin.getText().toString().trim().length() != 6) {
                    pinLayout.setError("Enter valid pincode");
                    validation = false;
                }

                if (validation) {
                    CreateAccountRequest request = new CreateAccountRequest();
                    CreateAccount accountRequest = new CreateAccount();
                    accountRequest.setAccount_master_group_id("");
                    accountRequest.setCompany_id(Integer.parseInt(ParameterConstant.COMPANY_ID));
                    accountRequest.setName(firstName.getText().toString() + "|" + lastName.getText().toString());
                    accountRequest.setMobile_number(mobile.getText().toString());
                    accountRequest.setEmail("");
                    accountRequest.setAddress(houseNo.getText().toString()
                            + "|" + apartment.getText().toString()
                            + "|" + street.getText().toString()
                            + "|" + landmark.getText().toString()
                            + "|" + area.getText().toString());
                    accountRequest.setCountry("India");
                    accountRequest.setState(state.getSelectedItem().toString());
                    accountRequest.setCity("");
                    accountRequest.setPincode(pin.getText().toString());
                    String user_id[] = {Preferences.getInstance(getApplicationContext()).getUserId()};
                    String branch_id[] = {"37"};
                    accountRequest.setUser_ids(user_id);
                    accountRequest.setSales_person_id(user_id);
                    accountRequest.setBranch_ids(branch_id);
                    request.setAccount(accountRequest);
                    if (id == null) {
                        apiCallBack(getApi().createAccount(request));
                    } else {
                        apiCallBack(getApi().updateAccount(request, id));
                    }

                }
            }
        });
        otherInput.setVisibility(View.GONE);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home.setBackground(getResources().getDrawable(R.drawable.input_view_bg3));
                office.setBackground(getResources().getDrawable(R.drawable.input_view_bg));
                other.setBackground(getResources().getDrawable(R.drawable.input_view_bg));
                otherInput.setVisibility(View.GONE);
            }
        });


        office.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home.setBackground(getResources().getDrawable(R.drawable.input_view_bg));
                office.setBackground(getResources().getDrawable(R.drawable.input_view_bg3));
                other.setBackground(getResources().getDrawable(R.drawable.input_view_bg));
                otherInput.setVisibility(View.GONE);
            }
        });


        other.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home.setBackground(getResources().getDrawable(R.drawable.input_view_bg));
                office.setBackground(getResources().getDrawable(R.drawable.input_view_bg));
                other.setBackground(getResources().getDrawable(R.drawable.input_view_bg3));

                otherInput.setVisibility(View.VISIBLE);
            }
        });




        /*CreateAccountRequest request = new CreateAccountRequest();
                CreateAccount accountRequest = new CreateAccount();
                accountRequest.setAccount_master_group_id(Preferences.getInstance(CreateUpdateAccountActivity.this).getAccountMasterGroupId());
                accountRequest.setCompany_id(Integer.parseInt(ParameterConstant.COMPANY_ID));
                accountRequest.setName(etName.getText().toString());
                accountRequest.setMobile_number(etMobile.getText().toString());
                accountRequest.setEmail(etEmail.getText().toString());
                accountRequest.setAddress(etAddress.getText().toString());
                accountRequest.setCountry(etCountry.getText().toString());
                accountRequest.setState(etState.getText().toString());
                accountRequest.setCity(etCity.getText().toString());
                accountRequest.setPincode(etPincode.getText().toString());
                String user_id[] = {Preferences.getInstance(getApplicationContext()).getUserId()};
                String branch_id[] = {"37"};
                accountRequest.setUser_ids(user_id);
                accountRequest.setSales_person_id(user_id);
                accountRequest.setBranch_ids(branch_id);
                request.setAccount(accountRequest);
                if (whereFrom.equals("Create")){
                    ApiCallService.action(CreateUpdateAccountActivity.this,request,ApiCallService.Action.ACTION_CREATE_ACCOUNT);
                }else {
                    ApiCallService.action(CreateUpdateAccountActivity.this,request,id,ApiCallService.Action.ACTION_UPDATE_ACCOUNT);
                }



               if(action.equals(Action.ACTION_CREATE_ACCOUNT)){
                   api.createAccount((CreateAccountRequest) request).enqueue(new Local<CreateAccountResponse>());
                }else if(action.equals(Action.ACTION_UPDATE_ACCOUNT)){
                   api.updateAccount((CreateAccountRequest) request,id).enqueue(new Local<UpdateAccountResponse>());
                }


        */
    }


    @Subscribe
    public void getAccountDetails(AccountDetailResponse response) {
        if (response.getStatus() == 200) {
            if (response.getAccount() != null) {
                UserAccountAttributes attributes = response.getAccount().getData().getAttributes();
                firstName.setText(attributes.getName().split("\\|")[0]);
                lastName.setText(attributes.getName().split("\\|")[1]);
                mobile.setText(attributes.getMobile_number());
                houseNo.setText(attributes.getAddress().split("\\|")[0]);
                apartment.setText(attributes.getAddress().split("\\|")[1]);
                street.setText(attributes.getAddress().split("\\|")[2]);
                landmark.setText(attributes.getAddress().split("\\|")[3]);
                area.setText(attributes.getAddress().split("\\|")[4]);
                state.setSelection(0);
                pin.setText(attributes.getPincode());
            }
        } else {
            getDialog(response.getMessage());
        }
    }


    @Subscribe
    public void success(SuccessResponse response) {
        if (response.getStatus()==200){
            Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
            finish();
        }else {
            getDialog(response.getMessage());
        }
    }
}
