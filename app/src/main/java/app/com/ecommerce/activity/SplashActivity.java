package app.com.ecommerce.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;

import java.util.ArrayList;
import java.util.List;

import app.com.ecommerce.R;
import app.com.ecommerce.activity.location_picker.PlacePickerActivity;
import app.com.ecommerce.utils.AppUser;
import app.com.ecommerce.utils.Helper;
import app.com.ecommerce.utils.Preferences;

public class SplashActivity extends AppCompatActivity {

    public static final int MULTIPLE_PERMISSIONS = 4;
    String[] permissions = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.CAMERA};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();
        if (AppUser.getAppUser(this) == null) {
            AppUser.saveAppUser(getApplicationContext(),  new AppUser());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            final int ACCESS_COARSE_LOCATION = PermissionChecker.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            final int ACCESS_FINE_LOCATION = PermissionChecker.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            final int WRITE_EXTERNAL_STORAGE = PermissionChecker.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            final int READ_EXTERNAL_STORAGE = PermissionChecker.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
            final int CALL_PHONE = PermissionChecker.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
            final int CAMERA = PermissionChecker.checkSelfPermission(this, Manifest.permission.CAMERA);
            if (ACCESS_COARSE_LOCATION == PermissionChecker.PERMISSION_GRANTED
                    && ACCESS_FINE_LOCATION == PermissionChecker.PERMISSION_GRANTED
                    && WRITE_EXTERNAL_STORAGE == PermissionChecker.PERMISSION_GRANTED
                    && READ_EXTERNAL_STORAGE == PermissionChecker.PERMISSION_GRANTED
                    && CALL_PHONE == PermissionChecker.PERMISSION_GRANTED
                    && CAMERA == PermissionChecker.PERMISSION_GRANTED) {
                launchActivity();
            } else {
                checkPermissions();
            }
        } else {
            launchActivity();
        }


    }


    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(SplashActivity.this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);

            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    launchActivity();
                } else {
                    launchActivity();
                }
                return;
            }
        }
    }


    void launchActivity() {
        if (Helper.isNetworkAvailable(getApplicationContext())){
//            ApiCallsService.action(getApplicationContext(),Cv.ACTION_VERSION);
        }else{
//            checkInternet();
        }


//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (Preferences.getInstance(getApplicationContext()).isLogin() == true) {
//                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent);
//                    finish();
//
//                } else {
//                    Intent intent = new Intent(getApplicationContext(), LandingActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent);
//                    finish();
//                }
//            }
//        }, 3000);

        if (Preferences.getInstance(getApplicationContext()).isLogin() == true) {
            if (Preferences.getInstance(getApplicationContext()).getLatitude().equals("")){
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
//                Intent intent = new Intent(getApplicationContext(), PlacePickerActivity.class);
                intent.putExtra("setup","yes");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }else {
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }


        } else {
            Intent intent = new Intent(getApplicationContext(), LandingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
    }

    /*@Subscribe
    public void version(VersionResponse response) {

        String versionName = "";


        if (response.isSuccess()) {
            try {
                PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                versionName = packageInfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            if (!versionName.equals(response.getApp_data().get(0).getVersionCode())) {
                android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(SplashActivity.this);
                alert.setCancelable(false);
                alert.setMessage("A new version of Serv Simplified Customer is available.Please update to version " + response.getApp_data().get(0).getVersionCode() + " now.");
                alert.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent i = new Intent(android.content.Intent.ACTION_VIEW);
                        i.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.servesimplified.customer"));
                        startActivity(i);
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (AppUser.getAppUser(getApplicationContext()).isAlreadyLoggedIn == true) {
                            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                            intent.putExtra("FROM","");
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();

                        } else {
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }
                });

                alert.show();
            } else {
                if (AppUser.getAppUser(getApplicationContext()).isAlreadyLoggedIn == true) {
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.putExtra("FROM","");
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();

                } else {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            }

        } else {
            // Toast.makeText(getApplicationContext(), versionResponse.getMessage(), Toast.LENGTH_LONG).show();
//            dialogMessage(getApplicationContext(),versionResponse.getMessage());
        }
    }*/







}
