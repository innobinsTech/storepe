package app.com.ecommerce.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.com.ecommerce.R;
import app.com.ecommerce.activity.base.BaseActivity;
import app.com.ecommerce.adapter.CartAdapter;
import app.com.ecommerce.checkout.TenderPayActivity;
import app.com.ecommerce.network.request.ItemCartRequest;
import app.com.ecommerce.network.resposne.SuccessResponse;
import app.com.ecommerce.network.resposne.cart.CreateItemCartResponse;
import app.com.ecommerce.network.resposne.item.Attributes;
import app.com.ecommerce.network.resposne.item.Data;
import app.com.ecommerce.network.resposne.item.GetItemResponse;
import app.com.ecommerce.utils.AppUser;
import app.com.ecommerce.utils.EventClick;
import app.com.ecommerce.utils.ParameterConstant;
import app.com.ecommerce.utils.Preferences;
import butterknife.BindView;
import butterknife.ButterKnife;
import cdflynn.android.library.checkview.CheckView;

public class CartActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.total)
    TextView total;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.totalLayout)
    LinearLayout totalLayout;
    @BindView(R.id.orderPlaced)
    LinearLayout orderPlaced;
    @BindView(R.id.check)
    CheckView check;
    @BindView(R.id.submitLayout)
    Button submitLayout;

    String limit = "50";
    int pageNo = 1;
    @BindView(R.id.selectAddress)
    LinearLayout selectAddress;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.select_date)
    TextView select_date;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.mobile)
    TextView mobile;
    @BindView(R.id.AddressLayout)
    LinearLayout AddressLayout;
    @BindView(R.id.changeAddress)
    LinearLayout changeAddress;
    @BindView(R.id.select_date_ll)
    LinearLayout select_date_ll;
    @BindView(R.id.deliveryType)
    RadioGroup deliveryType;
    @BindView(R.id.track)
    Button track;
    @BindView(R.id.noItem)
    LinearLayout noItem;
    private List<Data> itemsData;
    CartAdapter adapter;
    boolean loading = true;
    String selectedDeliveryType="Store Pickup";
    DatePickerDialog picker;
    String deliveryDateStr="";


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        initActionbar("CART");
        orderPlaced.setVisibility(View.GONE);
        final Calendar cldr = Calendar.getInstance();
        cldr.add(Calendar.DAY_OF_MONTH, 2);
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        select_date.setText(setDate(day,month,year));
        changeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AddressListActivity.class);
                intent.putExtra("intent", "intent");
                startActivityForResult(intent, 1);
            }
        });

        deliveryType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                RadioButton rb= (RadioButton) findViewById(checkedId);
                selectedDeliveryType = rb.getText().toString();
                if (!selectedDeliveryType.equals("Store Pickup")){
                    if (Preferences.getInstance(getApplicationContext()).getAddressId().equals("")) {
                        selectAddress.setVisibility(View.VISIBLE);
                        AddressLayout.setVisibility(View.GONE);
                        select_date_ll.setVisibility(View.VISIBLE);
                    } else {
                        selectAddress.setVisibility(View.GONE);
                        AddressLayout.setVisibility(View.VISIBLE);
                        select_date_ll.setVisibility(View.VISIBLE);

                    }
                }else {
                    selectAddress.setVisibility(View.GONE);
                    AddressLayout.setVisibility(View.GONE);
                    select_date_ll.setVisibility(View.GONE);

                }
            }
        });

        selectAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AddressListActivity.class);
                intent.putExtra("intent", "intent");
                startActivityForResult(intent, 1);
            }
        });

        track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),OrderHistoryActivity.class));
                finish();
            }
        });

        submitLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!selectedDeliveryType.equals("Store Pickup")){
                    if (Preferences.getInstance(getApplicationContext()).getAddressId().isEmpty()) {
                        getDialog("Please select address");
                        return;
                    }
                }
                Intent intent = new Intent(CartActivity.this, TenderPayActivity.class);
                intent.putExtra("selectedDeliveryType",selectedDeliveryType);
                intent.putExtra("total",total.getText().toString());
                intent.putExtra("delivery_date",deliveryDateStr);
                startActivity(intent);
            }
        });

        select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(CartActivity.this, new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                select_date.setText(setDate(dayOfMonth,  (monthOfYear + 1),year));
                                deliveryDateStr=select_date.getText().toString();
                            }
                        }, year, month, day);
                Calendar mcurrentDate = Calendar.getInstance();
                mcurrentDate.add(Calendar.DATE, 2);
                picker.getDatePicker().setMinDate(mcurrentDate.getTimeInMillis());
                picker.show();
            }
        });


//        submitLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (Preferences.getInstance(getApplicationContext()).getAddressId().isEmpty()) {
//                    getDialog("Please select address");
//                    return;
//                }
//                Map request = new HashMap();
//                Map voucher = new HashMap();
//                request.put("voucher", voucher);
//                voucher.put("payment_type", "");
//                voucher.put("account_master_id", Preferences.getInstance(getApplicationContext()).getAddressId());
//                voucher.put("mobile_number", "");
//                voucher.put("material_center_id", "");
//                List<Data> list = AppUser.getAppUser(getApplicationContext()).getItemsData();
//                List<Attributes> attributesList = new ArrayList<>();
//
//                for (int i = 0; i < list.size(); i++) {
//                    Attributes attr = list.get(i).getAttributes();
//                    attr.setItem_id(attr.getId());
//                    attr.setValue(attr.getQuantity()*attr.getSales_price_main());
//                    attr.setTotal(attr.getItemTotal());
//                    attr.setRate(attr.getSales_price_main());
//                    attr.setDiscount(0.0);
//                    attributesList.add(attr);
//                }
//
//                voucher.put("items", attributesList);
//                voucher.put("deliveryType", selectedDeliveryType);
////                voucher.put("items", AppUser.getAppUser(getApplicationContext()).getItemsData());
//                voucher.put("total", total.getText().toString().split("₹")[1]);
//                voucher.put("gross_total", total.getText().toString().split("₹")[1]);
//                voucher.put("payable_amount", total.getText().toString().split("₹")[1]);
//                voucher.put("paid_amount", total.getText().toString().split("₹")[1]);
//                voucher.put("previous_dues", "0.00");
//                voucher.put("used_credits", "false");
//                voucher.put("attachment", "");
//                voucher.put("discount_val", "");
//                voucher.put("without_tax", "false");
//                voucher.put("user_id", Preferences.getInstance(getApplicationContext()).getUserId());
//                voucher.put("mobile_company_user", Preferences.getInstance(getApplicationContext()).getUserId());
//                voucher.put("user_name", Preferences.getInstance(getApplicationContext()).getName());
//                voucher.put("billDiscount", "");
//                voucher.put("couponDiscount", "");
//                voucher.put("couponDiscountCode", "");
//                voucher.put("points_used", "");
//                voucher.put("points_amount", "");
//                voucher.put("branch_id", AppUser.getAppUser(getApplicationContext()).getSignUpAttribute().getBranches().get(0).getId());
//                apiCallBack(getApi().sale(ParameterConstant.COMPANY_ID, request));
//            }
//        });

//        Map map = new HashMap();
//        map.put("company_id", ParameterConstant.COMPANY_ID);
//        map.put("limit", limit);
//        map.put("page_no", "" + pageNo);
//        map.put("user_id", Preferences.getInstance(getApplicationContext()).getUserId());
//        apiCallBack(getApi().getItemsPagination(map));

        /*recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!recyclerView.canScrollVertically(1)) {
                    if (loading) {
                        progressBar.setVisibility(View.VISIBLE);
                        pageNo++;
                        Map map = new HashMap();
                        map.put("company_id", ParameterConstant.COMPANY_ID);
                        map.put("limit", limit);
                        map.put("page_no", "" + pageNo);
                        map.put("user_id", Preferences.getInstance(getApplicationContext()).getUserId());
                        apiCallBackWithout(getApi().getItemsPagination(map));
                    }
                }
            }
        });*/


    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!selectedDeliveryType.equals("Store Pickup")){
            if (Preferences.getInstance(getApplicationContext()).getAddressId().equals("")) {
                selectAddress.setVisibility(View.VISIBLE);
                AddressLayout.setVisibility(View.GONE);
            } else {
                selectAddress.setVisibility(View.GONE);
                AddressLayout.setVisibility(View.VISIBLE);
            }
        }else {
            selectAddress.setVisibility(View.GONE);
            AddressLayout.setVisibility(View.GONE);
        }

        name.setText(Preferences.getInstance(getApplicationContext()).getUserAddressName());
        mobile.setText(Preferences.getInstance(getApplicationContext()).getUserAddressMobile());
        address.setText(Preferences.getInstance(getApplicationContext()).getUserAddress());


        itemsData = new ArrayList<>();
        AppUser appUser = AppUser.getAppUser(getApplicationContext());
        List<Data> itemsDataDup = appUser.getItemsData();
        for (int i = 0; i < itemsDataDup.size(); i++) {
            if (itemsDataDup.get(i).getAttributes().getQuantity() != 0) {
                itemsData.add(itemsDataDup.get(i));
            }
        }
        recyclerView.setHasFixedSize(true);
        recyclerView.setFocusable(false);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        adapter = new CartAdapter(getApplicationContext(), itemsData);
        recyclerView.setAdapter(adapter);

        double d = 0;
        for (int i = 0; i < itemsData.size(); i++) {
            d = d + itemsData.get(i).getAttributes().getItemTotal();
        }
        total.setText("Total : ₹" + roundOff(d));

        if (itemsData.size() == 0) {
            deliveryType.setVisibility(View.GONE);
            submitLayout.setVisibility(View.GONE);
            totalLayout.setVisibility(View.GONE);
            selectAddress.setVisibility(View.GONE);
            AddressLayout.setVisibility(View.GONE);
            noItem.setVisibility(View.VISIBLE);
        } else {
            deliveryType.setVisibility(View.VISIBLE);
            submitLayout.setVisibility(View.VISIBLE);
            totalLayout.setVisibility(View.VISIBLE);
            noItem.setVisibility(View.GONE);
        }
    }

    /*@Subscribe
    public void getItemPaginate(GetItemResponse response) {
        progressBar.setVisibility(View.GONE);
        if (response.getStatus() == 200) {
            if (pageNo == 1) {
                itemsData = response.getItems().getData();
                recyclerView.setHasFixedSize(true);
                recyclerView.setFocusable(false);
                recyclerView.setNestedScrollingEnabled(true);
                recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
                adapter = new CartAdapter(getApplicationContext(), itemsData);
                recyclerView.setAdapter(adapter);
            } else {
                itemsData.addAll(response.getItems().getData());
                adapter.notifyDataSetChanged();
            }

        } else if (response.getStatus() == 204) {
            loading = false;
        } else {
            getDialog(response.getMessage());
        }
    }*/

    EventClick click;

    @Subscribe
    public void event(EventClick click) {
        this.click = click;
        ItemCartRequest request = new ItemCartRequest();
        request.setCompany_id(ParameterConstant.COMPANY_ID);
        request.setItem_id(itemsData.get(click.getPosition()).getId());
        request.setQuantity(itemsData.get(click.getPosition()).getQuantity());
        request.setUser_id(Preferences.getInstance(getApplicationContext()).getUserId());
        request.setPackaging_category_id("1");
        apiCallBackWithout(getApi().createItemCart(request));


    }


    @Subscribe
    public void createCart(CreateItemCartResponse response) {
        Attributes attributes = itemsData.get(click.getPosition()).getAttributes();
        attributes.setProgress(false);
        if (click.isAdd()) {
            attributes.setQuantity(attributes.getQuantity() + 1);
            attributes.setItemTotal(roundOff2(attributes.getItemTotal() + roundOff2(attributes.getSales_price_inclusive())));
            itemsData.get(click.getPosition()).setAttributes(attributes);
        } else {
            if (attributes.getQuantity() == 1) {
                attributes.setQuantity(0);
                attributes.setItemTotal(roundOff2(attributes.getItemTotal() - roundOff2(attributes.getSales_price_inclusive())));
                itemsData.get(click.getPosition()).setAttributes(attributes);
                itemsData.remove(click.getPosition());
            } else {
                attributes.setQuantity(attributes.getQuantity() - 1);
                attributes.setItemTotal(roundOff2(attributes.getItemTotal() - roundOff2(attributes.getSales_price_inclusive())));
                itemsData.get(click.getPosition()).setAttributes(attributes);
            }

        }

        double d = 0;
        for (int i = 0; i < itemsData.size(); i++) {
            d = d + itemsData.get(i).getAttributes().getItemTotal();
        }
        total.setText("Total : ₹" + roundOff(d));
        adapter.notifyDataSetChanged();

        AppUser appUser = AppUser.getAppUser(getApplicationContext());
        appUser.setItemsData(itemsData);
        AppUser.saveAppUser(getApplicationContext(), appUser);

        if (itemsData.size() == 0) {
            deliveryType.setVisibility(View.GONE);
            submitLayout.setVisibility(View.GONE);
            totalLayout.setVisibility(View.GONE);
            selectAddress.setVisibility(View.GONE);
            AddressLayout.setVisibility(View.GONE);
            noItem.setVisibility(View.VISIBLE);
        } else {
            deliveryType.setVisibility(View.VISIBLE);
            submitLayout.setVisibility(View.VISIBLE);
            totalLayout.setVisibility(View.VISIBLE);
            noItem.setVisibility(View.GONE);
        }
    }


    @Subscribe
    public void success(SuccessResponse response) {
        if (response.getStatus() == 200) {
            itemsData.clear();
            adapter.notifyDataSetChanged();
            orderPlaced.setVisibility(View.VISIBLE);
            submitLayout.setVisibility(View.GONE);
            totalLayout.setVisibility(View.GONE);
            AppUser appUser = AppUser.getAppUser(getApplicationContext());
            appUser.setItemsData(new ArrayList<>());
            AppUser.saveAppUser(getApplicationContext(), appUser);
            check.check();
        } else {
            getDialog(response.getMessage() + " " + response.getStatus());
        }
    }

    public String setDate(int day,int month,int year){
        String day1,month1;
        if(day<10){
            day1 = "0"+ day;
        }else {
            day1 = String.valueOf(day);
        }

        if(month<10){
            month1 = "0"+ month;
        }else {
            month1 = String.valueOf(month);
        }

        String date = day1+"/"+month1+"/"+year;
        return date;

    }


}
