package app.com.ecommerce.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;


import androidx.annotation.Nullable;

import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.Map;

import app.com.ecommerce.R;
import app.com.ecommerce.activity.base.BaseActivity;
import app.com.ecommerce.network.resposne.signup.SignUpResponse;
import app.com.ecommerce.utils.AppUser;
import app.com.ecommerce.utils.ParameterConstant;
import app.com.ecommerce.utils.Preferences;

public class LoginActivity extends BaseActivity {
    EditText mobile;
    LinearLayout submit;
    AppUser appUser;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        submit = findViewById(R.id.submit);
        mobile = findViewById(R.id.mobile);
        appUser = AppUser.getAppUser(this);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mobile.getText().toString().isEmpty()){
                    getDialog("Enter mobile number");
                    return;
                }
                if (mobile.getText().toString().isEmpty()){
                    getDialog("Enter mobile number");
                    return;
                }
                if (mobile.getText().toString().length()!=10){
                    getDialog("Enter valid mobile number");
                    return;
                }
                Map map = new HashMap();
                map.put("mobile",mobile.getText().toString());
                map.put("company_id", ParameterConstant.COMPANY_ID);
                map.put("client_token","");
                Map map1 = new HashMap();
                map1.put("user",map);
                apiCallBack(getApi().login(map1));
            }
        });



    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }





    @Subscribe
    public void timeOut(String message) {
       getDialog("Sorry",message);

    }

    @Subscribe
    public void response(SignUpResponse response){
        if (response.getStatus()==200){
            if (response.getMessage()==null){
                AppUser appUser=AppUser.getAppUser(getApplicationContext());
                if (appUser != null) {
                    appUser.setSignUpAttribute(response.getUser().getData().getAttributes());
                }
                AppUser.saveAppUser(getApplicationContext(),appUser);
                Preferences.getInstance(getApplicationContext()).setCashId(response.getCash_id());
                Preferences.getInstance(getApplicationContext()).setDelivery_charge(String.valueOf(response.getDelivery_charge()));
                Intent intent = new Intent(getApplicationContext(), OtpActivity.class);
                intent.putExtra("mobile",mobile.getText().toString());
                startActivity(intent);
            }else {
                getDialog(response.getMessage());
            }
        }else {
            getDialog(response.getMessage());
        }
    }
}