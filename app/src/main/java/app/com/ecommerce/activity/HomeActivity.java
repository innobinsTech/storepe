package app.com.ecommerce.activity;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;
import org.greenrobot.eventbus.Subscribe;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import app.com.ecommerce.R;
import app.com.ecommerce.activity.base.BaseActivity;
import app.com.ecommerce.adapter.ItemListAdapter;
import app.com.ecommerce.network.request.ItemCartRequest;
import app.com.ecommerce.network.request.ItemPaginationRequest;
import app.com.ecommerce.network.resposne.cart.CreateItemCartResponse;
import app.com.ecommerce.network.resposne.item.Attributes;
import app.com.ecommerce.network.resposne.item.Data;
import app.com.ecommerce.network.resposne.item.GetItemResponse;
import app.com.ecommerce.network.resposne.itemgroups.AccountGroupResponse;
import app.com.ecommerce.utils.AppUser;
import app.com.ecommerce.utils.EventClick;
import app.com.ecommerce.utils.MyProgressDialog;
import app.com.ecommerce.utils.ParameterConstant;
import app.com.ecommerce.utils.Preferences;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.ibrahimsn.lib.CirclesLoadingView;

public class HomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.loader)
    CirclesLoadingView loader;
    @BindView(R.id.total)
    TextView total;
    @BindView(R.id.submitLayout)
    LinearLayout submitLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;
    @BindView(R.id.changePlace)
    LinearLayout changePlace;
    @BindView(R.id.address)
    TextView address;

    String limit = "50";
    public static int pageNo = 1;
    @BindView(R.id.hamburger)
    LinearLayout hamburger;
    private List<Data> itemsData = new ArrayList<>();
    ItemListAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    boolean loading = true;
    int pastVisibleItems, visibleItemCount, totalItemCount;
    boolean isKeyPadOpen;

    SearchView searchView;
    LinearLayout checkout;
    TextView quantity_tv;
    TabLayout tabs;
    RelativeLayout root_layout;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_navigation);
        ButterKnife.bind(this);
        BaseActivity.context = this;
        navigation();
        changePlace.setVisibility(View.VISIBLE);
        quantity_tv = findViewById(R.id.quantity_tv);
        tabs = findViewById(R.id.tabs);
        tabs.setTabMode(TabLayout.MODE_SCROLLABLE);

        // Get search view and modify it to our needs
        searchView = (SearchView) findViewById(R.id.search_view);
        root_layout = findViewById(R.id.root_layout);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.onActionViewExpanded();
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;

            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(!newText.isEmpty()) {
                    ItemListAdapter.dataList = itemsData;
                    ItemListAdapter.filteredDataList = itemsData;
                    adapter.getFilter().filter(newText);
                }
                return false;
            }
        });
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        int searchPlateId = searchView.getContext().getResources().getIdentifier("android:id/search_plate", null, null);
        View searchPlate = searchView.findViewById(searchPlateId);
        searchPlate.setBackgroundResource(R.drawable.bg_white_rounded);

        int searchSrcTextId = getResources().getIdentifier("android:id/search_src_text", null, null);
        EditText searchEditText = (EditText) searchView.findViewById(searchSrcTextId);
        searchEditText.setTextColor(Color.BLACK);
        searchEditText.setHintTextColor(Color.LTGRAY);

        int closeButtonId = searchView.getContext().getResources().getIdentifier("android:id/search_close_btn", null, null);
        ImageView closeButtonImage = (ImageView) searchView.findViewById(closeButtonId);
        closeButtonImage.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        ///////////////searview////////////////
        KeyboardVisibilityEvent.setEventListener(HomeActivity.this, new KeyboardVisibilityEventListener() {
            @Override
            public void onVisibilityChanged(boolean isOpen) {
                isKeyPadOpen = isOpen;
            }
        });



//        address.setText(Preferences.getInstance(getApplicationContext()).getAddress());

        changePlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivityForResult(new Intent(getApplicationContext(), PlacePickerActivity.class), 1);
//                dialogTest();
                Intent intent = new Intent(getApplicationContext(), AddressListActivity.class);
                intent.putExtra("intent", "intent");
                startActivityForResult(intent, 1);
            }
        });


//        initActionbar("eCommerce", false);


        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    {
                        visibleItemCount = linearLayoutManager.getChildCount();
                        totalItemCount = linearLayoutManager.getItemCount();
                        pastVisibleItems = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                        if (loading) {
                            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                progressBar.setVisibility(View.VISIBLE);
                                pageNo++;
                                Map map = new HashMap();
                                map.put("company_id", ParameterConstant.COMPANY_ID);
                                map.put("limit", limit);
                                map.put("page_no", "" + pageNo);
                                map.put("user_id", Preferences.getInstance(getApplicationContext()).getUserId());
                                apiCallBackWithout(getApi().getItemsPagination(map));

                            }

                        }
                    }

                }
            }
        });


        submitLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Double.valueOf(total.getText().toString().split(" ₹")[1]) == 0) {
                    Toast.makeText(HomeActivity.this, "Please add item", Toast.LENGTH_SHORT).show();
                } else {
                    saveCart();
                    startActivity(new Intent(getApplicationContext(), CartActivity.class));
                }
            }
        });

        findViewById(R.id.checkout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(getApplicationContext(), CheckoutActivity.class);
//                startActivity(intent);

                if (Double.valueOf(total.getText().toString().split(" ₹")[1]) == 0) {
                    Toast.makeText(HomeActivity.this, "Please add item", Toast.LENGTH_SHORT).show();
                } else {
                    saveCart();
                    startActivity(new Intent(getApplicationContext(), CartActivity.class));
                }
            }
        });

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onResume() {
        searchView.setQuery("", false);
        root_layout.requestFocus();
        address.setText(Preferences.getInstance(getApplicationContext()).getUserAddress());
        super.onResume();
        loader.setVisibility(View.VISIBLE);
        itemsData.clear();
        Map map = new HashMap();
        map.put("company_id", ParameterConstant.COMPANY_ID);
        map.put("limit", limit);
        map.put("page_no", "" + pageNo);
        map.put("user_id", Preferences.getInstance(getApplicationContext()).getUserId());
        apiCallBackWithout(getApi().getItemsPagination(map));
        apiCallBackWithout(getApi().getItemGroup(ParameterConstant.COMPANY_ID));


        recyclerView.setHasFixedSize(true);
        recyclerView.setFocusable(false);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        adapter = new ItemListAdapter(getApplicationContext(), itemsData);
        recyclerView.setAdapter(adapter);
        double d = 0;
        for (int i = 0; i < itemsData.size(); i++) {
            d = d + itemsData.get(i).getAttributes().getItemTotal();
        }

        total.setText("Total : ₹" + roundOff(d));
        quantity_tv.setText(""+itemsData.size());


        int size = 0;
        for (int i = 0; i < itemsData.size(); i++) {
            if (itemsData.get(i).getAttributes().getQuantity() > 0) {
                size++;
                break;
            }
        }
        if (size > 0) {
            submitLayout.setVisibility(View.VISIBLE);
            recyclerView.setPadding(0, 0, 0, 200);
        } else {
            submitLayout.setVisibility(View.GONE);
            recyclerView.setPadding(0, 0, 0, 10);
        }
    }

    @Subscribe
    public void getItemPaginate(GetItemResponse response) {
        progressBar.setVisibility(View.GONE);
        loader.setVisibility(View.GONE);
        if (response.getStatus() == 200) {
            if (pageNo == 1) {
                itemsData = response.getItems().getData();
                recyclerView.setHasFixedSize(true);
                recyclerView.setFocusable(false);
                recyclerView.setNestedScrollingEnabled(true);
                linearLayoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(linearLayoutManager);
                adapter = new ItemListAdapter(getApplicationContext(), itemsData);
                recyclerView.setAdapter(adapter);
            } else {
                itemsData.addAll(response.getItems().getData());
                adapter.notifyDataSetChanged();
            }

        } else if (response.getStatus() == 204) {
            loading = false;
        } else {
            loading = false;
            getDialog(response.getMessage());
        }
    }

    EventClick click;

    @Subscribe
    public void event(EventClick click) {
        this.click = click;
        for (int i = 0; i < itemsData.size(); i++) {

               if (itemsData.get(i).getAttributes().getId().equals(click.getId())) {
                   ItemCartRequest request = new ItemCartRequest();
                   request.setCompany_id(ParameterConstant.COMPANY_ID);
                   request.setItem_id(itemsData.get(i).getId());
                   request.setQuantity(itemsData.get(i).getQuantity());
                   request.setUser_id(Preferences.getInstance(getApplicationContext()).getUserId());
                   request.setPackaging_category_id("1");
                   apiCallBackWithout(getApi().createItemCart(request));
               }

        }

    }


    @Subscribe
    public void createCart(CreateItemCartResponse response) {

        for (int i = 0; i < itemsData.size(); i++) {
            if (itemsData.get(i).getAttributes().getId().equals(click.getId())) {
                Attributes attributes = itemsData.get(i).getAttributes();
                attributes.setProgress(false);
                if (click.isAdd()) {
                    attributes.setQuantity(attributes.getQuantity() + 1);
                    attributes.setItemTotal(roundOff2(attributes.getItemTotal() + roundOff2(attributes.getSales_price_inclusive())));
                    itemsData.get(i).setAttributes(attributes);
                } else {
                    attributes.setQuantity(attributes.getQuantity() - 1);
                    attributes.setItemTotal(roundOff2(attributes.getItemTotal() - roundOff2(attributes.getSales_price_inclusive())));
                    itemsData.get(i).setAttributes(attributes);
                }
            }
        }


        double d = 0;
        for (int i = 0; i < itemsData.size(); i++) {
            d = d + itemsData.get(i).getAttributes().getItemTotal();
        }
        total.setText("Total : ₹" + roundOff(d));
        adapter.notifyDataSetChanged();

        int size = 0;
        for (int i = 0; i < itemsData.size(); i++) {
            if (itemsData.get(i).getAttributes().getQuantity() > 0) {
                size++;
                break;
            }
        }

        if (size > 0) {
            submitLayout.setVisibility(View.VISIBLE);
            recyclerView.setPadding(0, 0, 0, 200);
        } else {
            submitLayout.setVisibility(View.GONE);
            recyclerView.setPadding(0, 0, 0, 10);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_category, menu);
        return true;
    }


    @Subscribe
    public void getItemGroups(AccountGroupResponse response) {
        if (response.getStatus() == 200) {
            MyProgressDialog.setDismiss();

            for (int k = 0; k <response.getItem_groups().getData().size(); k++) {
                tabs.addTab(tabs.newTab().setText("" +response.getItem_groups().getData().get(k).getAttributes().name));
            }
            tabs.getTabAt(0).select();

            tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    HomeActivity.pageNo=1;
                    ItemPaginationRequest request = new ItemPaginationRequest();
                    request.setCompany_id(ParameterConstant.COMPANY_ID);
                    request.setLimit("" + "10");
                    request.setPage_no("" + "1");
                    request.setUser_id(Preferences.getInstance(context).getUserId());
                    request.setItem_group_id(String.valueOf(response.getItem_groups().getData().get(tab.getPosition()).getAttributes().getId()));
                    BaseActivity.context.apiCallBackWithout(BaseActivity.context.getApi().getItemsGroupBy(request));
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });




        } else {
            getDialog(response.getMessage());
        }
    }


    private ViewPager viewPager;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 200;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 2500; // time in milliseconds between successive task executions.
    int NUM_PAGES = 5;



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                address.setText(data.getStringExtra("address"));
                Preferences.getInstance(getApplicationContext()).setLatitude(data.getStringExtra("lat"));
                Preferences.getInstance(getApplicationContext()).setLongitude(data.getStringExtra("lon"));
                Preferences.getInstance(getApplicationContext()).setAddress(data.getStringExtra("address"));

            }
        }
    }


    DrawerLayout drawer;
    TextView name;
    TextView companyName;
    TextView mobile;
    ImageView image;

    void navigation() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        name = (TextView) header.findViewById(R.id.name);
        companyName = (TextView) header.findViewById(R.id.companyName);
        hamburger = findViewById(R.id.hamburger);
        mobile = (TextView) header.findViewById(R.id.mobile);
        image = header.findViewById(R.id.imageView);
        name.setText(Preferences.getInstance(getApplicationContext()).getName());
        mobile.setText(Preferences.getInstance(getApplicationContext()).getMobile());
        navigationView.setNavigationItemSelectedListener(this);
        drawer.closeDrawer(GravityCompat.START);

        hamburger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Helper.closeKeyPad(HomeActivity.this, isKeyPadOpen);
                drawer.openDrawer(GravityCompat.START);
            }
        });


    }


    public boolean onNavigationItemSelected(MenuItem item) {
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void profile(View view) {
//        AppUser.saveAppUser(getApplicationContext(), new AppUser());
        drawer.closeDrawer(GravityCompat.START);
    }

    public void menu(View view) {

        drawer.closeDrawer(GravityCompat.START);
    }


    public void address(View view) {
        startActivity(new Intent(getApplicationContext(), AddressListActivity.class));
        drawer.closeDrawer(GravityCompat.START);
    }

    public void purzi(View view) {
        drawer.closeDrawer(GravityCompat.START);
        startActivity(new Intent(getApplicationContext(), PurziActivity.class));
    }


    public void history(View view) {
        drawer.closeDrawer(GravityCompat.START);
        startActivity(new Intent(getApplicationContext(), OrderHistoryActivity.class));
    }


    public void share(View view) {
        drawer.closeDrawer(GravityCompat.START);
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
        String shareMessage = "Use eCommerce this app";
        shareMessage = shareMessage + " " + "http://play.google.com/store/apps/details?id=" + getPackageName();
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
        startActivity(Intent.createChooser(shareIntent, "choose one"));
    }


    public void playStore(View view) {
        drawer.closeDrawer(GravityCompat.START);
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }

    public void tnc(View view) {
        drawer.closeDrawer(GravityCompat.START);
    }

    public void cart(View view) {
        drawer.closeDrawer(GravityCompat.START);
        startActivity(new Intent(getApplicationContext(), CartActivity.class));
    }

    public void exit(View view) {
        drawer.closeDrawer(GravityCompat.START);
        Preferences.getInstance(getApplicationContext()).setLogin(false);
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    void saveCart() {
        AppUser appUser = AppUser.getAppUser(getApplicationContext());
        List<Data> dataList = appUser.getItemsData();
        List<Integer> positionArr = new ArrayList<>();
        for (int i = 0; i < itemsData.size(); i++) {
            if (itemsData.get(i).getAttributes().getQuantity() > 0) {
                for (int j = 0; j < dataList.size(); j++) {
                    if (dataList.get(j).getAttributes().getId().equals(itemsData.get(i).getAttributes().getId())) {
                        positionArr.add(i);
                        Data data = dataList.get(j);
                        data.getAttributes().setQuantity(data.getAttributes().getQuantity() + itemsData.get(i).getAttributes().getQuantity());
                        data.getAttributes().setItemTotal(roundOff2(data.getAttributes().getItemTotal() + roundOff2(itemsData.get(i).getAttributes().getItemTotal())));
                        dataList.set(j, data);
                    }
                }
            }
        }

        for (int i = 0; i < itemsData.size(); i++) {
            if (itemsData.get(i).getAttributes().getQuantity() > 0) {
                if (!positionArr.contains(i)) {
                    dataList.add(itemsData.get(i));
                }
            }
        }

        /*if (dataList.size() == 0) {
            for (int i = 0; i < itemsData.size(); i++) {
                if (itemsData.get(i).getAttributes().getQuantity() > 0) {
                    dataList.add(itemsData.get(i));
                }
            }
        }*/


        appUser.setItemsData(dataList);
        AppUser.saveAppUser(getApplicationContext(), appUser);
    }


}
