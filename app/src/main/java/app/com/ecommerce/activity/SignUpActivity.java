package app.com.ecommerce.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;


import androidx.annotation.RequiresApi;

import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.Map;

import app.com.ecommerce.R;
import app.com.ecommerce.activity.base.BaseActivity;
import app.com.ecommerce.network.resposne.signup.SignUpResponse;
import app.com.ecommerce.utils.AppUser;
import app.com.ecommerce.utils.ParameterConstant;
import app.com.ecommerce.utils.Preferences;

public class SignUpActivity extends BaseActivity {

    EditText name,mobile,pin;
    LinearLayout submit;
    AppUser appUser;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
      /*  getSupportActionBar().setTitle("Sign Up");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/
        name=findViewById(R.id.name);
        mobile=findViewById(R.id.mobile);
        pin=findViewById(R.id.pin);
        submit=findViewById(R.id.submit);
        appUser = AppUser.getAppUser(SignUpActivity.this);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().trim().isEmpty()){
                    getDialog("Sorry", "Please enter name");
                    return;
                }
                if (mobile.getText().toString().isEmpty()){
                    getDialog("Sorry", "Please enter name");
                    return;
                }
                if (mobile.getText().toString().length()!=10){
                    getDialog("Sorry","Enter valid mobile number");
                    return;
                }
                if (pin.getText().toString().isEmpty()){
                    getDialog("Sorry", "Please pin code");
                    return;
                }
                if (pin.getText().toString().length()!=6){
                    getDialog("Sorry","Enter valid pin code");
                    return;
                }
                Map map=new HashMap();
                map.put("name",name.getText().toString());
                map.put("mobile",mobile.getText().toString());
                map.put("postal_code",pin.getText().toString());
                map.put("company_id", ParameterConstant.COMPANY_ID);
                map.put("client_token","");
                apiCallBack(getApi().signup(map));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    public void register(View v) {
        Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_CLEAR_TASK);//***Change Here***
        startActivity(intent);
        finish();
    }

    @Subscribe
    public void response(SignUpResponse response){
        if (response.getStatus()==200){
                Preferences.getInstance(SignUpActivity.this).setToken(response.getUser().getData().getAttributes().getAuth_token());
                Preferences.getInstance(SignUpActivity.this).setName(response.getUser().getData().getAttributes().getName());
                Preferences.getInstance(SignUpActivity.this).setMobile(response.getUser().getData().getAttributes().getMobile());
                Preferences.getInstance(SignUpActivity.this).setUserId(response.getUser().getData().getId());
                Preferences.getInstance(SignUpActivity.this).setCompanyMobile(response.getUser().getData().getAttributes().getCompany_mobile_number());
                Intent intent = new Intent(getApplicationContext(), OtpActivity.class);
                intent.putExtra("mobile",mobile.getText().toString());
                startActivity(intent);
        }else {
            getDialog(response.getMessage());
        }
    }
}
