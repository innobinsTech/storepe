package app.com.ecommerce.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.print.PdfPrint;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import app.com.ecommerce.R;
import app.com.ecommerce.activity.base.ApiCallActivity;
import app.com.ecommerce.network.resposne.SuccessResponse;
import app.com.ecommerce.utils.Helper;
import app.com.ecommerce.utils.Preferences;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PrintActivity extends ApiCallActivity {

    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.webViewShow)
    WebView webViewShow;
    @BindView(R.id.print)
    RelativeLayout print;
    @BindView(R.id.footericon)
    ImageView footericon;
    @BindView(R.id.done)
    LinearLayout done;
    @BindView(R.id.sms)
    RelativeLayout sms;
    @BindView(R.id.email)
    RelativeLayout email;
    @BindView(R.id.whatsApp)
    RelativeLayout whatsApp;

    @BindView(R.id.a4)
    RelativeLayout a4;
    @BindView(R.id.inch3)
    RelativeLayout inch3;
    @BindView(R.id.inch4)
    RelativeLayout inch4;

    private ProgressDialog progressDialog;


    String mobileNumber;
    private String type = "";


    String invoiceA4;
    String invoice3Inch;
    String invoice4Inch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Print");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Processing...");
        mobileNumber = getIntent().getStringExtra("mobile");
        if (mobileNumber==null){
            mobileNumber="";
        }

        invoiceA4=getIntent().getStringExtra("company_report");
        invoice3Inch=getIntent().getStringExtra("three");
        invoice4Inch=getIntent().getStringExtra("four");
        printView(invoice3Inch);

        a4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printView(invoiceA4);
            }
        });

        inch3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printView(invoice3Inch);
            }
        });

        inch4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printView(invoice4Inch);
            }
        });

        print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createWebPrintJob(webView, false);
            }
        });

        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "Sms";
                apiCallBack(getApi().invoiceNotification(getIntent().getStringExtra("id"), "Estimate", type));
            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "Email";
                apiCallBack(getApi().invoiceNotification(getIntent().getStringExtra("id"), "Estimate", type));
            }
        });
        whatsApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "WhatsApp";
                createWebPrintJob(webView, true);
            }
        });


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void createWebPrintJob(WebView webView, final boolean isWhatsApp) {
        String jobName = getString(R.string.app_name) + " Document";
        PrintAttributes attributes = new PrintAttributes.Builder()
                .setMediaSize(PrintAttributes.MediaSize.ISO_A4)
                .setResolution(new PrintAttributes.Resolution("pdf", "pdf", 500, 500))
                .setMinMargins(PrintAttributes.Margins.NO_MARGINS).build();
        final File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS + "/" + Helper.FOLDER_NAME + "/");
        PdfPrint pdfPrint = new PdfPrint(attributes);
        PrintDocumentAdapter adapter;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            adapter = webView.createPrintDocumentAdapter(jobName);
        } else {
            adapter = webView.createPrintDocumentAdapter();
        }
        pdfPrint.print(adapter, path, "invoice.pdf");
        progressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
                File pdf = new File(path.toString() + "/invoice.pdf");
                if (!isWhatsApp) {
                    if (pdf.exists()) {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_SEND);
                        intent.putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(PrintActivity.this, PrintActivity.this.getPackageName() + ".provider", pdf));
                        intent.setType("application/pdf");
                        startActivity(intent);
                    } else {
                        Log.i("DEBUG", "File doesn't exist");
                    }
                } else {
                    if (pdf.exists()) {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_SEND);
                        //                    intent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                        if (mobileNumber.contains("+")) {
                            mobileNumber = mobileNumber.replace("+", "");
                        }
                        if (!mobileNumber.startsWith("91")) {
                            mobileNumber = "91".concat(mobileNumber);
                        }
                        intent.putExtra("jid", PhoneNumberUtils.stripSeparators(mobileNumber) + "@s.whatsapp.net");
                        intent.setType("application/pdf");
                        intent.putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(PrintActivity.this, PrintActivity.this.getPackageName() + ".provider", pdf));
                        intent.setPackage("com.whatsapp");
                        startActivity(intent);
                    } else {
                        Log.i("DEBUG", "File doesn't exist");
                    }
                }
            }
        }, 3 * 1000);
    }


    void printView(String company_report) {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        webView.loadDataWithBaseURL(null, company_report, "text/html", "utf-8", null);
        webViewShow.loadDataWithBaseURL(null, company_report, "text/html", "utf-8", null);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Picture picture = view.capturePicture();
                            Bitmap b = Bitmap.createBitmap(picture.getWidth(), picture.getHeight(), Bitmap.Config.ARGB_8888);
                            Canvas c = new Canvas(b);
                            picture.draw(c);
                            webView.setDrawingCacheEnabled(true);
                            footericon.setImageBitmap(b);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, 1000);
            }
        });
    }




    @Subscribe
    public void notification(SuccessResponse response) {
        if (response.getStatus() == 200) {
            Toast.makeText(this, type + " sent successfully", Toast.LENGTH_SHORT).show();
        } else {
            getDialog(response.getMessage());
        }
    }

}
