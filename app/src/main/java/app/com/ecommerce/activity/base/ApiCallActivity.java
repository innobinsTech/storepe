package app.com.ecommerce.activity.base;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import app.com.ecommerce.R;
import app.com.ecommerce.network.Api;
import app.com.ecommerce.network.ThisApp;
import app.com.ecommerce.utils.Helper;
import app.com.ecommerce.utils.MyProgressDialog;
import app.com.ecommerce.utils.ParameterConstant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ApiCallActivity<T> extends AppCompatActivity {

    private static final String NO_INTERNET_TITTLE = "No Internet";
    //    Call<T> callback;
    Api api;
    public static ApiCallActivity context;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        api = ThisApp.getApi(getApplicationContext());
        try {
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }


    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe
    public void timeOut(String msg) {
        getDialogFailed(msg);
    }


    public Api getApi() {
        return ThisApp.getApi(this);
    }

    public void apiCallBack(final Call<T> callback) {
//        this.callback=callback;
        if (!Helper.isNetworkAvailable(this)) {
            new AlertDialog.Builder(this)
                    .setTitle(ParameterConstant.NO_INTERNET_TITTLE)
                    .setMessage(ParameterConstant.NO_INTERNET_MESSAGE)
                    .setCancelable(false)
                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            apiCallBack(callback);
                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .setIcon(R.mipmap.ic_launcher)
                    .show();

        } else {
            try {
                MyProgressDialog.getInstance(this).show();
                callback.enqueue(new Local<T>());
            }catch (Exception e){
                callback.enqueue(new Local<T>());
                e.printStackTrace();
            }

        }

    }


    public void apiCallBackWithout(final Call<T> callback) {
//        this.callback=callback;
        if (!Helper.isNetworkAvailable(this)) {
            new AlertDialog.Builder(this)
                    .setTitle(ParameterConstant.NO_INTERNET_TITTLE)
                    .setMessage(ParameterConstant.NO_INTERNET_MESSAGE)
                    .setCancelable(false)
                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            apiCallBackWithout(callback);
                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .setIcon(R.mipmap.ic_launcher)
                    .show();

        } else {
            try {
                callback.enqueue(new LocalWithout<T>());
            }catch (Exception e){
                e.printStackTrace();
            }

        }

    }



    public void getDialogFailed(String message) {
        new AlertDialog.Builder(this)
                .setTitle("Sorry")
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })

//                .setNegativeButton("Cancel", null)
                .setIcon(R.mipmap.ic_launcher)
                .show();
    }

    /*void getDialog( String tittle, String message) {
        new AlertDialog.Builder(this)
                .setTitle(tittle)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        apiCallBack(callback);
                    }
                })

//                .setNegativeButton("Exit", null)
                .setIcon(R.drawable.without_m)
                .show();
    }*/


    class Local<T> implements Callback<T> {

        @Override
        public void onResponse(Call<T> call, Response<T> response) {
            MyProgressDialog.setDismiss();
            if (response.code() == 200) {
                T body = response.body();
                EventBus.getDefault().post(body);
            } else {
                EventBus.getDefault().post(ParameterConstant.ERROR + " " + response.code());
            }
        }

        @Override
        public void onFailure(Call<T> call, Throwable t) {
            MyProgressDialog.setDismiss();
            EventBus.getDefault().post(t.getMessage());
        }
    }



    class LocalWithout<T> implements Callback<T> {

        @Override
        public void onResponse(Call<T> call, Response<T> response) {
            MyProgressDialog.setDismiss();
            if (response.code() == 200) {
                T body = response.body();
                EventBus.getDefault().post(body);
            } else {
                EventBus.getDefault().post(ParameterConstant.ERROR + " " + response.code());
            }
        }

        @Override
        public void onFailure(Call<T> call, Throwable t) {
            MyProgressDialog.setDismiss();
            EventBus.getDefault().post(t.getMessage());
        }
    }


    public void getDialog(String tittle, String message) {
        new AlertDialog.Builder(this)
                .setTitle(tittle)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(R.drawable.logo)
                .show();
    }

    public void getDialog(String message) {
        new AlertDialog.Builder(this)
                .setTitle("Sorry")
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(R.drawable.logo)
                .show();
    }



    public void getDialogSuccess(String message) {
        new AlertDialog.Builder(this)
                .setTitle("Success")
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(R.drawable.logo)
                .show();
    }




    public double getValue(TextView textView) {
        if (textView.getText().toString().isEmpty()) {
            return 0.0;
        } else {
            return Double.valueOf(textView.getText().toString());
        }
    }


    public double getValue(String textView) {
        return Double.valueOf(textView);
    }

    public double getValue(EditText textView) {
        if (textView.getText().toString().equals("null")) {
            return 0.0;
        }if (textView.getText().toString().trim().isEmpty()) {
            return 0.0;
        } else {
            return Double.valueOf(textView.getText().toString());
        }
    }



    public String roundOff(double val) {
        return String.format("%.1f", val);
    }

    public Double roundOff2(double val) {
        String str=String.format("%.1f", val);
        return Double.valueOf(str);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }
}
