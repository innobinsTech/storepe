package app.com.ecommerce.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import app.com.ecommerce.R;
import app.com.ecommerce.activity.base.BaseActivity;
import app.com.ecommerce.adapter.PurziAdapter;
import app.com.ecommerce.network.Purzi;
import app.com.ecommerce.utils.BuzzTextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PurziActivity extends BaseActivity {

    @BindView(R.id.itemName)
    TextInputEditText itemName;
    @BindView(R.id.itemNameLayout)
    TextInputLayout itemNameLayout;
    @BindView(R.id.brand)
    TextInputEditText brand;
    @BindView(R.id.brandLayout)
    TextInputLayout brandLayout;
    @BindView(R.id.description)
    TextInputEditText description;
    @BindView(R.id.descriptionLayout)
    TextInputLayout descriptionLayout;
    @BindView(R.id.unit)
    Spinner unit;
    @BindView(R.id.quantity)
    TextInputEditText quantity;
    @BindView(R.id.quantityLayout)
    TextInputLayout quantityLayout;
    @BindView(R.id.addOrder)
    LinearLayout addOrder;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.submit)
    LinearLayout submit;

    PurziAdapter adapter;
    @BindView(R.id.comment)
    TextView comment;
    @BindView(R.id.commentLayout)
    LinearLayout commentLayout;

    List<Purzi> purziList;
    List<TextView> viewList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purzi);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Purzi");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView.setHasFixedSize(true);
        recyclerView.setFocusable(false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        purziList = new ArrayList<>();
        viewList = new ArrayList<>();
        viewList.add(itemName);
        viewList.add(brand);
        viewList.add(description);
        viewList.add(quantity);
        Collections.reverse(purziList);
        adapter = new PurziAdapter(this, purziList,viewList);
        recyclerView.setAdapter(adapter);

        commentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogTest(comment);
            }
        });

        removeError(itemName,itemNameLayout);
        removeError(quantity,quantityLayout);

        addOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean validation = true;
                if (itemName.getText().toString().trim().isEmpty()) {
                    itemNameLayout.setError("Enter item");
                    validation = false;
                }


                if (quantity.getText().toString().trim().isEmpty()) {
                    quantityLayout.setError("Enter quantity/rupees");
                    validation = false;
                }

                if (validation) {
                    Purzi purzi = new Purzi();
                    purzi.setName(itemName.getText().toString());
                    purzi.setBrand(brand.getText().toString());
                    purzi.setDescription(description.getText().toString());
                    purzi.setUnit(unit.getSelectedItem().toString());
                    purzi.setQuantity(quantity.getText().toString());
                    purziList.add(purzi);
                    Collections.reverse(purziList);
                    adapter.notifyDataSetChanged();
                    itemName.setText("");
                    brand.setText("");
                    description.setText("");
                    quantity.setText("");
                    unit.setSelection(0);
                    itemName.requestFocus();
                }
            }
        });

    }


    void dialogTest(TextView comment) {

        for (int i=0;i<viewList.size();i++){
            viewList.get(i).clearFocus();
        }

        final BottomSheetDialog d  = new BottomSheetDialog(this);
        Objects.requireNonNull(d.getWindow()).setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        d.setContentView(R.layout.dialog_price);
        d.getBehavior().setHideable(false);
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        EditText editText=d.findViewById(R.id.editText);
        TextView submit=d.findViewById(R.id.submit);
        editText.requestFocus();
        editText.setText(comment.getText().toString());
        editText.setSelection(editText.getText().toString().length());
        if (editText.getText().toString().isEmpty()){
            submit.setTextColor(getResources().getColor(R.color.default_text));
        }else {
            submit.setTextColor(getResources().getColor(R.color.blue));
        }

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editText.getText().toString().isEmpty()){
                    submit.setTextColor(getResources().getColor(R.color.default_text));
                }else {
                    submit.setTextColor(getResources().getColor(R.color.blue));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment.setText(editText.getText().toString());
                d.dismiss();
            }
        });
        d.show();
    }

    void  removeError(TextInputEditText editText,TextInputLayout layout){
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                layout.setError(null);
            }
        });

    }
}
