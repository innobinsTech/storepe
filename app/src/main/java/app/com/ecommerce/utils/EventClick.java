package app.com.ecommerce.utils;

public class EventClick {
    String  id;
    int  position;
    boolean isAdd;

    public EventClick(String id,boolean isAdd){
        this.id=id;
        this.isAdd=isAdd;
    }


    public EventClick(int position,boolean isAdd){
        this.position=position;
        this.isAdd=isAdd;
    }

    public String getId() {
        return id;
    }

public int getPosition() {
        return position;
    }


    public boolean isAdd() {
        return isAdd;
    }


}
