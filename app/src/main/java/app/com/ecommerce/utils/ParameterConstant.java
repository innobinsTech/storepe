package app.com.ecommerce.utils;

public interface ParameterConstant {
    String M_SCHOOLING = "mSchooling";
    String NO_INTERNET_TITTLE = "No Internet";
    String NO_INTERNET_MESSAGE = "Please check your internet connection!!!";
    String COMPANY_ID = "1";
    String ABOUT_US_URL = "http://baniyabazzar.com/index.php/about-us/";
    String ERROR = "Some thing went wrong";
    String CHANNEL_ID = "my_channel_01";
    String CHANNEL_NAME = "Simplified Coding Notification";
    String CHANNEL_DESCRIPTION = "www.simplifiedcoding.net";
    String BASE_URL = "http://34.93.190.253";
    String BASE_IMAGE_URL = "http://34.93.190.253/";
}
