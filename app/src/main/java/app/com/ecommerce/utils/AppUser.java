package app.com.ecommerce.utils;



import android.content.Context;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import app.com.ecommerce.network.resposne.item.Data;
import app.com.ecommerce.network.resposne.signup.SignUpAttribute;

public class AppUser {

    static AppUser appUser;
    public static AppUser getInstance(){
        if (AppUser.appUser==null){
            appUser=new AppUser();
        }
        return appUser;
    }



    static String PREFS_APP_USER = "com.baniyabazzar";

    public synchronized static void saveAppUser(Context ctx, AppUser user) {

        String jsonString = new Gson().toJson(user);
        PreferenceManager.getDefaultSharedPreferences(ctx)
                .edit()
                .putString(PREFS_APP_USER, jsonString)
                .commit();

    }

    public synchronized static AppUser getAppUser(Context ctx) {

        String jsonString = PreferenceManager.getDefaultSharedPreferences(ctx)
                .getString(PREFS_APP_USER, "");

        return "".equals(jsonString) ?
                null : new Gson().fromJson(jsonString, AppUser.class);
    }



    private List<Data> itemsData=new ArrayList<>();

    public List<Data> getItemsData() {
        return itemsData;
    }

    public void setItemsData(List<Data> itemsData) {
        this.itemsData = itemsData;
    }


    SignUpAttribute signUpAttribute;

    public SignUpAttribute getSignUpAttribute() {
        return signUpAttribute;
    }

    public void setSignUpAttribute(SignUpAttribute signUpAttribute) {
        this.signUpAttribute = signUpAttribute;
    }
}
