package app.com.ecommerce.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by pc on 10/7/2016.
 */
public class Preferences {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    private static Preferences instance;

    private static final String BASE_URL = "base_url";
    private static final String login = "login";
    private static final String token = "token";
    private static final String name = "name";
    private final String mobile = "mobile";
    private final String CODE = "code";
    private final String SCHOOL_ID = "school_id";
    private final String pageNo1 = "pageNo1";
    private final String pageNo2 = "pageNo2";
    private final String userId="userId";
    private final String firstTimeAdminLogin = "firstTimeAdminLogin";
    private final String classId = "classId";
    private final String teacherId = "teacherId";
    private static final String pos_sale_type="pos_sale_type";
    private static final String pos_sale_type_id="pos_sale_type_id";
    private static final String orderDate="orderDate";
    private static final String partyId="partyId";
    private static final String accountMasterGroupId="accountMasterGroupId";
    private static final String companyMobile="companyMobile";
    private static final String branchId="branchId";
    private static final String salesPersonsId="salesPersonsId";
    private static final String statusId="statusId";
    private static final String materialCenterId="materialCenterId";
    private static final String latitude="latitude";
    private static final String longitude="longitude";
    private static final String address="address";
    private static final String addressId="addressId";
    private static final String userAddress="userAddress";
    private static final String userAddressName="userAddressName";
    private static final String userAddressMobile="userAddressMobile";
    private static final String cash_id="cash_id";
    private static final String delivery_charge="delivery_charge";

    private static final String startDate = "startDate";
    private static final String endDate = "endDate";


    private Preferences(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences("My_pref", 0);
        editor = pref.edit();
    }


    public static Preferences getInstance(Context context) {
        if (instance == null) {
            instance = new Preferences(context);
        }
        return instance;
    }


    public void setToken(String cname) {
        editor.putString(token, cname);
        editor.commit();
    }

    public String getToken() {
        return pref.getString(token, "");
    }


    public void setLogin(Boolean cname) {
        editor.putBoolean(login, cname);
        editor.commit();
    }

    public boolean isLogin() {
        return pref.getBoolean(login, false);
    }


    public void setName(String cname) {
        editor.putString(name, cname);
        editor.commit();
    }

    public String getName() {
        return pref.getString(name, "");
    }

    public void setMobile(String role) {
        editor.putString(mobile, role);
        editor.commit();
    }

    public String getMobile() {
        return pref.getString(mobile, "");
    }

    public void setBaseUrl(String baseUrl) {
        editor.putString(BASE_URL, baseUrl);
        editor.commit();
    }

    public String getBaseUrl() {
        return pref.getString(BASE_URL, "");
    }


    public void setCode(String cname) {
        editor.putString(CODE, cname);
        editor.commit();
    }

    public String getCode() {
        return pref.getString(CODE, "");
    }

    public void setPageNo1(int cname) {
        editor.putInt(pageNo1, cname);
        editor.commit();
    }

    public int getPageNo1() {
        return pref.getInt(pageNo1, 0);
    }


    public void setPageNo2(int cname) {
        editor.putInt(pageNo2, cname);
        editor.commit();
    }

    public int getPageNo2() {
        return pref.getInt(pageNo2, 0);
    }

    public void setSchoolId(String cname) {
        editor.putString(SCHOOL_ID, cname);
        editor.commit();
    }

    public String getSchoolId() {
        return pref.getString(SCHOOL_ID, "");
    }

public void setUserId(String cname) {
        editor.putString(userId, cname);
        editor.commit();
    }

    public String getUserId() {
        return pref.getString(userId, "");
    }


    public void setClassId(String cname) {
        editor.putString(classId, cname);
        editor.commit();
    }

    public String getClassId() {
        return pref.getString(classId, "");
    }


    public void setTeacherId(String cname) {
        editor.putString(teacherId, cname);
        editor.commit();
    }

    public String getTeacherId() {
        return pref.getString(teacherId, "");
    }


    public void setFirstTimeAdminLogin(int cname) {
        editor.putInt(firstTimeAdminLogin, cname);
        editor.commit();
    }

    public int getFirstTimeAdminLogin() {
        return pref.getInt(firstTimeAdminLogin, 0);
    }

    public void setPos_sale_type(String posSaleType) {
        editor.putString(pos_sale_type, posSaleType);
        editor.commit();
    }

    public String getPos_sale_type() {
        return pref.getString(pos_sale_type, "");
    }

    public void setPos_sale_type_id(String posSaleId) {
        editor.putString(pos_sale_type_id, posSaleId);
        editor.commit();
    }

    public String getPos_sale_type_id() {
        return pref.getString(pos_sale_type_id, "");
    }

  public void setOrderDate(String orderDate1) {
        editor.putString(orderDate, orderDate1);
        editor.commit();
    }

    public String getOrderDate() {
        return pref.getString(orderDate, "");
    }


public void setPartyId(String partyId1) {
        editor.putString(partyId, partyId1);
        editor.commit();
    }

    public String getPartyId() {
        return pref.getString(partyId, "");
    }

public void setAccountMasterGroupId(String masterId) {
        editor.putString(accountMasterGroupId, masterId);
        editor.commit();
    }

    public String getAccountMasterGroupId() {
        return pref.getString(accountMasterGroupId, "");
    }

    public void setCompanyMobile(String companyMobile1) {
        editor.putString(companyMobile, companyMobile1);
        editor.commit();
    }

    public String getCompanyMobile() {
        return pref.getString(companyMobile, "");
    }

    public void setBranchId(String branchId1) {
        editor.putString(branchId, branchId1);
        editor.commit();
    }

    public String getBranchId() {
        return pref.getString(branchId, "");
    }


    public void setLatitude(String materialCenterI1) {
        editor.putString(latitude, materialCenterI1);
        editor.commit();
    }

    public String getLatitude() {
        return pref.getString(latitude, "");
    }


    public void setLongitude(String statusId1) {
        editor.putString(longitude, statusId1);
        editor.commit();
    }

    public String getLongitude() {
        return pref.getString(longitude, "");
    }


    public void setAddress(String salesPersonsId1) {
        editor.putString(address, salesPersonsId1);
        editor.commit();
    }

    public String getAddress() {
        return pref.getString(address, "");
    }



    public void setUserAddress(String salesPersonsId1) {
        editor.putString(userAddress, salesPersonsId1);
        editor.commit();
    }

    public String getUserAddress() {
        return pref.getString(userAddress, "");
    }





    public void setUserAddressName(String salesPersonsId1) {
        editor.putString(userAddressName, salesPersonsId1);
        editor.commit();
    }

    public String getUserAddressName() {
        return pref.getString(userAddressName, "");
    }




    public void setUserAddressMobile(String salesPersonsId1) {
        editor.putString(userAddressMobile, salesPersonsId1);
        editor.commit();
    }

    public String getUserAddressMobile() {
        return pref.getString(userAddressMobile, "");
    }




    public void setAddressId(String salesPersonsId1) {
        editor.putString(addressId, salesPersonsId1);
        editor.commit();
    }

    public String getAddressId() {
        return pref.getString(addressId, "");
    }

    public void setStartDate(String cname) {
        editor.putString(startDate, cname);
        editor.commit();
    }

    public String getStartDate() {
        return pref.getString(startDate, "");
    }

    public void setEndDate(String cname) {
        editor.putString(endDate, cname);
        editor.commit();
    }

    public String getEndDate() {
        return pref.getString(endDate, "");
    }

    public void setCashId(int cname) {
        editor.putInt(cash_id, cname);
        editor.commit();
    }

    public int getCashId() {
        return pref.getInt(cash_id, -1);
    }

    public void setDelivery_charge(String cname) {
        editor.putString(delivery_charge, cname);
        editor.commit();
    }

    public String getDelivery_charge() {
        return pref.getString(delivery_charge, "");
    }




}